import os
import re
import sys

import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset
from torch.utils.data import ConcatDataset, random_split
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from collections import Counter
import matplotlib.pyplot as plt

from pytorch_lightning import LightningDataModule
from data_plotting import plot_coordinates
from data_plotting import plot_pca
from features import create_coordinates_matrix
from features import create_coordinates_matrix_p24
from features import create_features_matrix
from features import build_fully_connected_graph
from features import _preprocess_image
from features import build_pyg_graph_temporal
from features import normalize_image
from torch_geometric.data import Data
from torch_geometric.data import DataLoader
from features import build_coord_mat
from features import augment_coord_mat
from features import build_rgb_sequence
from features import build_rgb_sequence_2
from features import adjust_coord_matrix
import random
from random import shuffle
from torch.utils.data import Subset
from torch.utils.data import WeightedRandomSampler
from features import align_plane_and_rotate
from features import rotation_matrix
from features import swap_markers
import json


class MultiModalData(Data):
    def __init__(self, x_graph=None, edge_index=None, edge_attr=None, y=None, x_image=None, x_coord=None, x_feature=None):
        super(MultiModalData, self).__init__(x=x_graph, edge_index=edge_index, edge_attr=edge_attr, y=y)
        self.x_image = x_image
        self.x_coord = x_coord
        self.x_feature = x_feature


class RatBehaviorDataset(Dataset):
    def __init__(self, dataset_path, start_time, gap_time, fps, plot_data, num_markers,
                 graph_type, excluded_labels, model_type, seq_length, apply_transforms=False, p24=False):
        self.time_ranges = []

        self.dataset_path = dataset_path
        self.start_time = start_time
        self.gap_time = gap_time
        self.fps = fps
        self.sequence_length = seq_length
        self.plot_data = plot_data
        self.num_markers = num_markers
        self.apply_transforms = apply_transforms
        self.graph_type = graph_type
        self.model_type = model_type

        print("data:", dataset_path, "graphs: ", self.graph_type)
        self.excluded_labels = excluded_labels
        self.p24 = p24

        # Plane

        arena_corners = np.array([
            [-311.517677765096, 40.2822930457915, 949.255098395399],
            [166.829668699406, 36.2070176477812, 979.625206494983],
            [193.732915175345, 313.440593621476, 582.513110520185],
            [-274.070068351027, 315.574241893707, 543.672604868204]
        ])

        v1 = arena_corners[1] - arena_corners[0]
        v2 = arena_corners[3] - arena_corners[0]

        # Normal to it
        normal = np.cross(v1, v2)
        normal = -normal / np.linalg.norm(normal)

        # Rotation
        rotation_axis = np.cross(normal, [0, 0, 1])
        rotation_axis = rotation_axis / np.linalg.norm(rotation_axis)
        angle = np.arccos(np.dot(normal, [0, 0, 1]))

        self.rot_mat = rotation_matrix(rotation_axis, angle)
        self.aligned_corners = np.dot(self.rot_mat, arena_corners.T).T

        if not self.p24:
            if self.model_type == 'coord' or self.model_type == 'iniciator':
                self.coordinates = self.load_coordinates()
            if self.model_type == 'features' or self.model_type == 'all' or self.model_type == 'all_late':
                self.features = self.load_features()
                self.coordinates = self.load_coordinates()
            # Label aggregation:
            with open(r'assets\aggregate_labels_mounting_iniciator.json', 'r') as f:
                self.label_aggregation = json.load(f)

            # Load labels
            self.labels = self.load_labels()
            self.labels = self.labels[~self.labels['label'].isin(self.excluded_labels)]
            print(len(self.labels))
        else:
            self.apply_transforms = False
            if self.model_type == 'coord':
                self.labels = self.load_labels_p24()
                print(len(self.labels))
            else:
                print("coord mode needed for p24")
                sys.exit()

    def _process_image(self, start_frame, end_frame):
        views_seq = []
        for view in range(1, 5):
            view_seq = []
            rand_state = 0
            do_flip = 0
            if self.apply_transforms and random.random() > 0.5:
                rotation_degree = random.uniform(-15, 15)
                brightness_factor = random.uniform(0.85, 1.15)
                contrast_factor = random.uniform(0.85, 1.15)
                rand_state = 1
                if random.random() > 0.5:
                    do_flip = 1

            for im in range(start_frame, end_frame+1):
                if rand_state == 1:
                    view_seq_image = self._load_image(im, view, True, rotation_degree, brightness_factor, contrast_factor, do_flip)
                else:
                    view_seq_image = self._load_image(im, view)
                view_seq.append(view_seq_image)
            views_seq.append(torch.stack(view_seq))

            # print(f"Shape of view images: {np.array(view_seq).shape}")
        all_views_seq = torch.stack(views_seq)

        return all_views_seq

    def _process_cuboid(self, start_frame, end_frame):

        views_seq = []
        for view in range(1, 5):
            view_seq = []
            prev_image = self._load_image(start_frame, view)

            for im in range(start_frame+1, end_frame + 1):

                current_image = self._load_image(im, view)
                motion_image = torch.abs(current_image - prev_image)
                # print(motion_image.shape)
                motion_image = motion_image - motion_image.min()
                motion_image = motion_image / motion_image.max()
                view_seq.append(motion_image)
                prev_image = current_image

                # image_path = os.path.join('pca_plot/cuboid', f'cub{start_frame}_{im}_{view}.png')
                # plt.imsave(image_path, motion_imagesave)
            views_seq.append(torch.stack(view_seq))

        all_views_seq = torch.stack(views_seq)

        return all_views_seq

    def _load_image(self, frame_idx, cam_idx, apply_augmentation=False, rotation_degree=0.0, brightness_factor=1.0, contrast_factor=1.0, do_flip=0):
        images_path = os.path.join(self.dataset_path, 'model_data', 'images')
        image_path = os.path.join(images_path, f'{frame_idx}_{cam_idx}.jpg')
        img_x = _preprocess_image(image_path, apply_augmentation, rotation_degree, brightness_factor, contrast_factor, do_flip)
        return img_x

    def load_coordinates(self):
        coordinates_path = os.path.join(self.dataset_path, 'model_data', 'deep_coordinates_3D')
        coordinates_files = [f for f in os.listdir(coordinates_path) if f.endswith('.csv')]
        coordinates_data = {}

        for file in coordinates_files:
            full_path = os.path.join(coordinates_path, file)
            df = pd.read_csv(full_path, header=None)
            coordinates_data[file] = df

        self.time_ranges = list(set(re.findall(r'(\d+_\d+)_coordinates_3D_rat\d.csv', f)[0]
                                    for f in coordinates_files))

        return coordinates_data

    def load_features(self):
        features_path = os.path.join(self.dataset_path, 'model_data', 'features_deep')
        features_files = [f for f in os.listdir(features_path) if f.endswith('.csv')]
        features_data = {}

        for file in features_files:
            full_path = os.path.join(features_path, file)
            df = pd.read_csv(full_path)
            features_data[file] = df

        self.time_ranges = list(set(re.findall(r'(\d+_\d+)_features.csv', f)[0]
                                             for f in features_files))

        return features_data

    def aggregate_label(self, original_label):
        for new_label, original_labels in self.label_aggregation.items():
            if original_label in original_labels:
                return int(new_label)
        return original_label

    def load_labels_p24(self):
        labels_p24 = pd.read_csv(os.path.join(self.dataset_path, 'markerDataset.csv'), header=None, skiprows=1)

        for col in range(78, 150):
            min_val = labels_p24[col].min()
            max_val = labels_p24[col].max()
            labels_p24[col] = 2 * ((labels_p24[col] - min_val) / (max_val - min_val)) - 1

        labels_nan = labels_p24.iloc[:, -1].values
        labels = pd.DataFrame([int(x) for x in np.nan_to_num(labels_nan)])
        valid_data = pd.DataFrame()
        for i in range(1, len(labels_p24) - self.sequence_length-1, self.sequence_length):
            sub_data = labels_p24.iloc[i:i + self.sequence_length, :-1]
            sub_labels = labels.iloc[i:i + self.sequence_length, -1].values
            if len(set(sub_labels)) == 1:
                sub_data['label'] = sub_labels
                valid_data = pd.concat([valid_data, sub_data])

        valid_data.reset_index(drop=True, inplace=True)
        return valid_data

    def load_labels(self):
        labels_path = os.path.join(self.dataset_path, 'model_data', 'set_labels_deep')
        detailed_labels = pd.read_csv(os.path.join(labels_path, 'detailed_labels.csv'), header=None)
        detailed_labels.columns = ['label', 'start_frame', 'end_frame', 'iniciator']
        detailed_labels['label'] = detailed_labels['label'].apply(self.aggregate_label)
        exclude_nan_labels = []
        for idx, label_row in detailed_labels.iterrows():
            start_frame, end_frame = label_row['start_frame'], label_row['end_frame']
            start_time = start_frame / self.fps
            end_time = end_frame / self.fps
            for time_range in self.time_ranges:
                start, end = map(int, time_range.split('_'))
                if start <= start_time and end >= end_time:
                    rat1_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat1.csv"]
                    rat2_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat2.csv"]
                    rat1_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']
                    rat2_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']

                    coordinate_csv_start_idx = start_frame - start * self.fps
                    coordinate_csv_end_idx = end_frame - start * self.fps

                    rat1_range = rat1_coordinates[rat1_coordinates['Frame'].between(coordinate_csv_start_idx, coordinate_csv_end_idx)]
                    rat2_range = rat2_coordinates[rat2_coordinates['Frame'].between(coordinate_csv_start_idx, coordinate_csv_end_idx)]

                    if rat1_range.isnull().values.any() or rat2_range.isnull().values.any():
                        exclude_nan_labels.append(idx)
                    break
        print('From:', self.dataset_path, f'removing {len(exclude_nan_labels)}')
        detailed_labels = detailed_labels.drop(index=exclude_nan_labels).reset_index(drop=True)

        return detailed_labels

    def __len__(self):
        if not self.p24:
            return len(self.labels)
        else:
            return (len(self.labels) // self.sequence_length) - 1

    def __getitem__(self, idx):
        # print("IDX:", idx)
        label_row = 0
        idx_multi = 0
        data = 0

        if self.model_type == 'coord' or self.model_type == 'iniciator':
            if not self.p24:
                time_range = ''
                label_row = self.labels.iloc[idx]
                start_frame, end_frame = label_row['start_frame'], label_row['end_frame']
                start_time = start_frame / self.fps
                end_time = end_frame / self.fps
                start = 0
                for time_range in self.time_ranges:
                    start, end = map(int, time_range.split('_'))
                    if start <= start_time and end >= end_time:
                        break

                rat1_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat1.csv"]
                rat1_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']
                rat2_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat2.csv"]
                rat2_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']

                coordinate_csv_start_idx = start_frame - start * self.fps
                coordinate_csv_end_idx = end_frame - start * self.fps

                coord_mat_rat1 = create_coordinates_matrix(rat1_coordinates, coordinate_csv_start_idx,
                                                           coordinate_csv_end_idx, self.num_markers)

                coord_mat_rat2 = create_coordinates_matrix(rat2_coordinates, coordinate_csv_start_idx,
                                                           coordinate_csv_end_idx, self.num_markers)
            else:
                idx_multi = idx * self.sequence_length
                rat1_coordinates = self.labels.iloc[idx_multi:idx_multi + self.sequence_length, 78:114]
                rat2_coordinates = self.labels.iloc[idx_multi:idx_multi + self.sequence_length, 114:150]
                coord_mat_rat1 = create_coordinates_matrix_p24(rat1_coordinates, self.sequence_length, self.num_markers)
                coord_mat_rat2 = create_coordinates_matrix_p24(rat2_coordinates, self.sequence_length, self.num_markers)

            if self.plot_data:
                plot_coordinates(coord_mat_rat1, self.num_markers, f'coord_plot/coordinate_plot{idx}.png')

            coord_mat, aligned_corners = align_plane_and_rotate(build_coord_mat(coord_mat_rat1, coord_mat_rat2),
                                                                self.rot_mat, self.aligned_corners)

            if self.apply_transforms and random.random() > 0.5:
                coord_mat = augment_coord_mat(coord_mat, aligned_corners)
            swap_state = 0
            if self.apply_transforms and random.random() > 0.5:
                coord_mat = swap_markers(coord_mat, self.num_markers)
                swap_state = 1

            if self.graph_type != 'rgb' and self.graph_type != 'rgb2':
                x, edge_spatial, edge_weight_spatial, edge_temporal, edge_weight_temporal = build_pyg_graph_temporal(
                    coord_mat, self.graph_type)

                if not self.p24:
                    data = Data(x=x,
                                edge_index_spatial=edge_spatial,
                                edge_weight_spatial=edge_weight_spatial,
                                edge_index_temporal=edge_temporal,
                                edge_weight_temporal=edge_weight_temporal,
                                y=torch.tensor(label_row['label'], dtype=torch.long))
                else:
                    data = Data(x=x,
                                edge_index_spatial=edge_spatial,
                                edge_weight_spatial=edge_weight_spatial,
                                edge_index_temporal=edge_temporal,
                                edge_weight_temporal=edge_weight_temporal,
                                y=torch.tensor(self.labels.iloc[idx_multi, -1], dtype=torch.long))
            elif self.graph_type == 'rgb' and self.model_type != 'iniciator':
                x = build_rgb_sequence(coord_mat)
                y = torch.tensor(label_row['label'], dtype=torch.long)
                data = Data(x=torch.tensor(np.array(x)),
                            y=y)
            elif self.graph_type == 'rgb2':
                x = build_rgb_sequence_2(coord_mat, self.aligned_corners)
                y = torch.tensor(label_row['label'], dtype=torch.long)
                data = Data(x=torch.tensor(np.array(x)),
                            y=y)
            elif self.model_type == 'iniciator' and self.graph_type == 'rgb':
                x = build_rgb_sequence(coord_mat)
                y = label_row['iniciator']
                if y == 1 and swap_state == 0:
                    y = 0
                elif y == 1 and swap_state == 1:
                    y = 1
                if y == 2 and swap_state == 0:
                    y = 1
                elif y == 2 and swap_state == 1:
                    y = 0
                y = torch.tensor(y, dtype=torch.long)

                data = Data(x=torch.tensor(np.array(x)),
                            y=y)
        elif self.model_type == 'image':
            label_row = self.labels.iloc[idx]
            start_frame, end_frame = label_row['start_frame'], label_row['end_frame']

            x = self._process_image(start_frame, end_frame)
            y = torch.tensor(label_row['label'], dtype=torch.long)
            data = Data(x=x,
                        y=y)
        elif self.model_type == 'cuboid':
            label_row = self.labels.iloc[idx]
            start_frame, end_frame = label_row['start_frame'], label_row['end_frame']

            x = self._process_cuboid(start_frame, end_frame)
            y = torch.tensor(label_row['label'], dtype=torch.long)
            data = Data(x=x,
                        y=y)
        elif self.model_type == 'features':
            time_range = ''
            label_row = self.labels.iloc[idx]

            start_frame, end_frame = label_row['start_frame'], label_row['end_frame']
            start_time = start_frame / self.fps
            end_time = end_frame / self.fps
            start = 0

            for time_range in self.time_ranges:

                start, end = map(int, time_range.split('_'))
                if start <= start_time and end >= end_time:
                    break

            rat_features = self.features[f"{time_range}_features.csv"]
            rat_features.columns = ['H2H', 'N2T', 'T2N', 'S1', 'S2']

            features_csv_start_idx = start_frame - start * self.fps
            features_csv_end_idx = end_frame - start * self.fps

            features_mat = create_features_matrix(rat_features, features_csv_start_idx,
                                                  features_csv_end_idx, 5)
            y = torch.tensor(label_row['label'], dtype=torch.long)
            data = Data(x=torch.tensor(np.array(features_mat)),
                        y=y)
        if self.model_type == 'all':
            if not self.p24:
                time_range = ''
                label_row = self.labels.iloc[idx]
                start_frame, end_frame = label_row['start_frame'], label_row['end_frame']
                start_time = start_frame / self.fps
                end_time = end_frame / self.fps
                start = 0
                for time_range in self.time_ranges:
                    start, end = map(int, time_range.split('_'))
                    if start <= start_time and end >= end_time:
                        break

                rat1_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat1.csv"]
                rat1_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']
                rat2_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat2.csv"]
                rat2_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']

                coordinate_csv_start_idx = start_frame - start * self.fps
                coordinate_csv_end_idx = end_frame - start * self.fps

                coord_mat_rat1 = create_coordinates_matrix(rat1_coordinates, coordinate_csv_start_idx,
                                                           coordinate_csv_end_idx, self.num_markers)

                coord_mat_rat2 = create_coordinates_matrix(rat2_coordinates, coordinate_csv_start_idx,
                                                           coordinate_csv_end_idx, self.num_markers)
                coord_mat, aligned_corners = align_plane_and_rotate(build_coord_mat(coord_mat_rat1, coord_mat_rat2),
                                                                    self.rot_mat, self.aligned_corners)

                if self.apply_transforms and random.random() > 0.5:
                    coord_mat = augment_coord_mat(coord_mat, aligned_corners)

                coord_mat_graph = adjust_coord_matrix(coord_mat, self.aligned_corners)
                x_graph, edge_spatial, edge_weight_spatial, edge_temporal, edge_weight_temporal = build_pyg_graph_temporal(
                    coord_mat_graph, self.graph_type)

                x_rgb = torch.tensor(np.array(build_rgb_sequence(coord_mat)))

                y = torch.tensor(label_row['label'], dtype=torch.long)

                x_images = self._process_image(start_frame, end_frame)

                rat_features = self.features[f"{time_range}_features.csv"]
                rat_features.columns = ['H2H', 'N2T', 'T2N', 'S1', 'S2']

                features_csv_start_idx = start_frame - start * self.fps
                features_csv_end_idx = end_frame - start * self.fps

                x_features = torch.tensor(np.array(create_features_matrix(rat_features, features_csv_start_idx,
                                          features_csv_end_idx, 5)))
                data = Data(x=x_graph,
                            edge_index_spatial=edge_spatial,
                            edge_weight_spatial=edge_weight_spatial,
                            edge_index_temporal=edge_temporal,
                            edge_weight_temporal=edge_weight_temporal,
                            x_image=x_images,
                            x_coord=x_rgb,
                            x_feature=x_features,
                            y=y)
        if self.model_type == 'all_late':
            if not self.p24:
                time_range = ''
                label_row = self.labels.iloc[idx]
                start_frame, end_frame = label_row['start_frame'], label_row['end_frame']
                start_time = start_frame / self.fps
                end_time = end_frame / self.fps
                start = 0
                for time_range in self.time_ranges:
                    start, end = map(int, time_range.split('_'))
                    if start <= start_time and end >= end_time:
                        break

                rat1_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat1.csv"]
                rat1_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']
                rat2_coordinates = self.coordinates[f"{time_range}_coordinates_3D_rat2.csv"]
                rat2_coordinates.columns = ['Frame', 'Marker', 'XYZ', 'Coordinates']

                coordinate_csv_start_idx = start_frame - start * self.fps
                coordinate_csv_end_idx = end_frame - start * self.fps

                coord_mat_rat1 = create_coordinates_matrix(rat1_coordinates, coordinate_csv_start_idx,
                                                           coordinate_csv_end_idx, self.num_markers)

                coord_mat_rat2 = create_coordinates_matrix(rat2_coordinates, coordinate_csv_start_idx,
                                                           coordinate_csv_end_idx, self.num_markers)
                coord_mat, aligned_corners = align_plane_and_rotate(build_coord_mat(coord_mat_rat1, coord_mat_rat2),
                                                                    self.rot_mat, self.aligned_corners)

                if self.apply_transforms and random.random() > 0.5:
                    coord_mat = augment_coord_mat(coord_mat, aligned_corners)

                if self.apply_transforms and random.random() > 0.5:
                    coord_mat = swap_markers(coord_mat, self.num_markers)

                x_rgb = torch.tensor(np.array(build_rgb_sequence(coord_mat)))

                y = torch.tensor(label_row['label'], dtype=torch.long)

                x_images = self._process_image(start_frame, end_frame)

                data = Data(x_image=x_images,
                            x_coord=x_rgb,
                            y=y)

        return data


class RatBehaviorDataModule(LightningDataModule):
    def __init__(self, datasets, start_time, gap_time, fps, batch_size, plot_data, num_markers, graph_type,
                 excluded_labels, model_type, sequence_length, p24=False):
        super().__init__()
        self.val_dataset = None
        self.train_dataset = None
        self.datasets = datasets
        self.start_time = start_time
        self.gap_time = gap_time
        self.fps = fps
        self.batch_size = batch_size
        self.plot_data = plot_data
        self.num_markers = num_markers
        self.graph_type = graph_type
        self.excluded_labels = excluded_labels
        self.p24 = p24
        self.model_type = model_type
        self.sequence_length = sequence_length

    def setup(self, stage=None):

        train_datasets = [RatBehaviorDataset(ds, self.start_time, self.gap_time, self.fps, self.plot_data,
                                             self.num_markers,
                                             self.graph_type, self.excluded_labels, self.model_type,
                                             self.sequence_length,
                                             apply_transforms=True, p24=self.p24) for ds in self.datasets]

        val_datasets = [RatBehaviorDataset(ds, self.start_time, self.gap_time, self.fps, self.plot_data,
                                           self.num_markers,
                                           self.graph_type, self.excluded_labels,
                                           self.model_type, self.sequence_length,
                                           apply_transforms=False, p24=self.p24) for ds in self.datasets]

        concatenated_dataset_train = ConcatDataset(train_datasets)
        concatenated_dataset_val = ConcatDataset(val_datasets)

        # total_samples = len(concatenated_dataset_train)
        labels = [data.y.item() for data in concatenated_dataset_train]
        print('Total datasets size:', len(labels))
        label_counts = Counter(labels)

        for label, count in label_counts.items():
            print(f'Label {label}: {count} occurrences')

        seed = 42
        train_indices, val_indices = train_test_split(
            np.arange(len(labels)),
            test_size=0.2,
            random_state=seed,
            stratify=labels
        )

        self.train_dataset = Subset(concatenated_dataset_train, train_indices)
        self.val_dataset = Subset(concatenated_dataset_val, val_indices)

        print('Total training datasets size:', len(self.train_dataset))
        print('Total validation datasets size:', len(self.val_dataset))

    def get_sampler(self, dataset):
        labels = [data.y.item() for data in dataset]

        class_counts = {}
        for label in labels:
            if label in class_counts:
                class_counts[label] += 1
            else:
                class_counts[label] = 1

        total_samples = len(labels)

        adjustment_power = 0.85
        class_weights = {label: (total_samples / (class_counts[label] ** adjustment_power)) for label in class_counts}

        weight_sum = sum(class_weights.values())
        class_weights = {label: weight / weight_sum for label, weight in class_weights.items()}

        sample_weights = [class_weights[label] for label in labels]

        sampler = WeightedRandomSampler(sample_weights, num_samples=len(sample_weights), replacement=True)

        return sampler

    def train_dataloader(self):
        train_sampler = self.get_sampler(self.train_dataset)
        return DataLoader(self.train_dataset, batch_size=self.batch_size, sampler=train_sampler, drop_last=True,
                          num_workers=1)

    def val_dataloader(self):
        val_sampler = self.get_sampler(self.val_dataset)
        return DataLoader(self.val_dataset, batch_size=self.batch_size, sampler=val_sampler, drop_last=True,
                          num_workers=1)
