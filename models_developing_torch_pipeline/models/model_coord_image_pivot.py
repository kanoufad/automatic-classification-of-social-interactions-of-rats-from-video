import torch
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
from torch_geometric.nn import GCNConv
from torch_geometric.nn import GATConv
from pytorch_lightning import LightningModule
from torch_geometric.nn import global_mean_pool
from ignite.metrics import Accuracy, Precision, Recall
from ignite.utils import to_onehot
from torchvision import models

from sklearn.metrics import classification_report
from torch.utils.tensorboard import SummaryWriter
from pytorch_lightning.loggers import TensorBoardLogger
import torchmetrics
import gc
import matplotlib.pyplot as plt


def flatten_dict(d, parent_key='', sep='_'):
    items = {}
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.update(flatten_dict(v, new_key, sep=sep))
        else:
            items[new_key] = v
    return items


class SkeletonCNN(nn.Module):
    def __init__(self):
        super(SkeletonCNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(p=0.5)

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.pool(self.relu(self.conv2(x)))
        return x


def plot_confusion_matrix(cm, filename):
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.colormaps['Blues'])
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           ylabel='True label',
           xlabel='Predicted label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    plt.savefig(filename)
    plt.close(fig)


class CNN_coord_pivot(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(CNN_coord_pivot, self).__init__()
        self.skeleton_cnn = SkeletonCNN()

        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len
        self.train_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')
        self.val_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')

        example_input = torch.zeros((1, 3, 15, 9))
        example_output = self.skeleton_cnn(example_input)
        cnn_output_size = example_output.numel()

        self.fc1 = nn.Linear(2 * cnn_output_size, 128)
        self.fc2 = nn.Linear(128, num_classes)
        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x):
        x = x.float()

        batch_size = int(x.size(0)/self.seq_len)
        # print('here:', x.shape)
        x = x.view(batch_size, self.seq_len, self.num_markers*2, 3)
        # print(x.shape)
        x = x.permute(0, 3, 1, 2)
        # print(x.shape)
        order_images = torch.chunk(x, chunks=2, dim=3)
        # print(len(order_images))
        # print(order_images[0].shape)
        cnn_outputs = [self.skeleton_cnn(order_images[i]) for i in range(2)]
        # print(cnn_outputs[0].shape)
        concat_images = torch.cat(cnn_outputs, dim=1)
        # print(concat_images.shape)
        x = concat_images.reshape(batch_size, -1)
        # print('concat:', x.shape)
        x = F.dropout(F.relu(self.fc1(x)), p=0.3, training=self.training)
        x = self.fc2(x)

        return x

    def training_step(self, batch, batch_idx):
        # print(batch.shape)
        x = batch.x
        # print(x.shape)
        y = batch.y
        # print(y.shape)
        output = self(x)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('Train', accuracy)

        self.log('train_loss', loss)
        self.log('train_accuracy', accuracy, on_epoch=True, prog_bar=True)
        self.train_confusion_matrix.update(preds, y)

        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'train_reported_{name}' if name == 'accuracy' else f'train_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        # print(batch.shape)
        x = batch.x
        # print(x.shape)
        y = batch.y
        output = self(x)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('VAL', accuracy)
        self.log('val_loss', loss)
        self.log('val_accuracy', accuracy, on_epoch=True, prog_bar=True)
        self.val_confusion_matrix.update(preds, y)

        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'val_reported_{name}' if name == 'accuracy' else f'val_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'val_loss': loss}

    def on_train_epoch_end(self):
        cm = self.train_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/coord_rgb_pivot/train_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.train_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def on_validation_epoch_end(self):
        cm = self.val_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/coord_rgb_pivot/val_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.val_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        lr_scheduler = {
            'scheduler': torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10,
                                                                    verbose=True),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1,
            'strict': True,
        }
        return {'optimizer': optimizer, 'lr_scheduler': lr_scheduler}
