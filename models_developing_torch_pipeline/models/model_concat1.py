import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv
from pytorch_lightning import LightningModule
from torch_geometric.nn import global_mean_pool
from ignite.metrics import Accuracy, Precision, Recall
from ignite.utils import to_onehot
from sklearn.metrics import classification_report
from torch.utils.tensorboard import SummaryWriter
from torchvision import models
import torchmetrics
import matplotlib.pyplot as plt
import numpy as np
import gc


def flatten_dict(d, parent_key='', sep='_'):
    items = {}
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.update(flatten_dict(v, new_key, sep=sep))
        else:
            items[new_key] = v
    return items


class SkeletonCNN(nn.Module):
    def __init__(self):
        super(SkeletonCNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(p=0.5)

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.pool(self.relu(self.conv2(x)))
        return x


class CNN_mod(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(CNN_mod, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len

        self.resnet = models.resnet18(pretrained=True)
        self.resnet.fc = nn.Identity()
        self.image_lstm = nn.LSTM(4*512, 512, batch_first=True)


    def forward(self, x):
        images = x
        # print('shape model:', images.shape)
        batch_size_four_times, seq_length, c, h, w = images.shape
        images = images.view(batch_size_four_times * seq_length, c, h, w)
        x = self.resnet(images)
        # print(x.shape)
        x = x.view(batch_size_four_times, seq_length, -1)
        batch_size = batch_size_four_times // 4
        x = x.view(batch_size, 4, seq_length, -1)
        # print(x.shape)
        x = x.permute(0, 2, 1, 3)
        # print(x.shape)
        x = x.reshape(batch_size, seq_length, -1)
        # print(x.shape)
        x, _ = self.image_lstm(x)
        x = torch.mean(x, dim=1)
        x = F.dropout(x, p=0.2, training=self.training)

        return x


class CNN_coord(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(CNN_coord, self).__init__()
        self.skeleton_cnn = SkeletonCNN()

        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len

        example_input = torch.zeros((1, 3, 15, 9))
        example_output = self.skeleton_cnn(example_input)
        cnn_output_size = example_output.numel()

        self.fc1 = nn.Linear(6 * cnn_output_size, 128)
        self.fc2 = nn.Linear(128, num_classes)
        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x):
        batch_size = int(x.size(0)/self.seq_len)
        # print('here:', x.shape)
        x = x.view(batch_size, self.seq_len, self.num_markers*6, 3)
        # print(x.shape)
        x = x.permute(0, 3, 1, 2)
        # print(x.shape)
        order_images = torch.chunk(x, chunks=6, dim=3)
        # print(order_images[0].shape)
        cnn_outputs = [self.skeleton_cnn(order_images[i]) for i in range(6)]
        # print(cnn_outputs[0].shape)
        concat_images = torch.cat(cnn_outputs, dim=1)
        # print(concat_images.shape)
        x = concat_images.reshape(batch_size, -1)
        # print('concat:', x.shape)

        return x


class FEAT_mod(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(FEAT_mod, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len

        self.convs = nn.ModuleList(
            [nn.Conv1d(in_channels=1, out_channels=16, kernel_size=3, padding=1) for _ in range(num_features)])

        self.fc1 = nn.Linear(16 * num_features * seq_len, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, num_classes)

        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x):
        # x shape: [batch_size, seq_len, num_features]
        x = x.float()
        # print(x.shape)
        x = x.view(self.batch_size, self.seq_len, -1)
        # print(x.shape)

        x = x.permute(0, 2, 1).unsqueeze(2)  # shape: [batch_size, num_features, 1, seq_len]

        conv_outs = [F.relu(conv(x[:, i, :, :])) for i, conv in enumerate(self.convs)]
        x = torch.cat(conv_outs, dim=1)

        x = torch.flatten(x, start_dim=1)

        return x


class ST_GCN_simple(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(ST_GCN_simple, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len

        # Spatial stream
        self.spatial_conv1 = GCNConv(num_features, 128)
        self.spatial_conv2 = GCNConv(128, 384)

        # Temporal stream
        self.temporal_conv1 = GCNConv(num_features, 64)
        self.temporal_conv2 = GCNConv(64, 128)

        self.fc1 = torch.nn.Linear(512, 128)
        self.fc2 = torch.nn.Linear(128, 64)

        self.fc3 = torch.nn.Linear(64, num_classes)

        self.adaptive_avg_pool = nn.AdaptiveAvgPool1d(256)
        self.lstm_comm = nn.LSTM(self.num_markers * 2 * 128, 256, 1, batch_first=True)
        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, batch_ids):
        # Spatial stream
        # print("input shape:", x.shape)
        x_spatial = F.relu(self.spatial_conv1(x, edge_index_spatial))
        # print('gcnn spat1', x_spatial.shape)
        x_spatial = F.dropout(x_spatial, p=0.2, training=self.training)
        x_spatial = F.relu(self.spatial_conv2(x_spatial, edge_index_spatial))
        # print(x_spatial.shape)
        out_spatial = global_mean_pool(x_spatial, batch_ids)

        x_temporal = F.relu(self.temporal_conv1(x, edge_index_temporal))
        x_temporal = F.dropout(x_temporal, p=0.2, training=self.training)
        x_temporal = F.relu(self.temporal_conv2(x_temporal, edge_index_temporal))

        x_temp_global = global_mean_pool(x_temporal, batch_ids)

        x = torch.cat([out_spatial, x_temp_global], dim=1)

        return x


def plot_confusion_matrix(cm, filename):
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.colormaps['Blues'])
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           ylabel='True label',
           xlabel='Predicted label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    plt.savefig(filename)
    plt.close(fig)


class MultiMod(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(MultiMod, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len

        self.train_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')
        self.val_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')

        self.skeleton_cnn = SkeletonCNN()
        self.cnn_coord = CNN_coord(num_features=3, num_classes=num_classes, learning_rate=learning_rate,
                                   num_markers=num_markers, batch_size=batch_size, seq_len=seq_len)
        self.cnn_mod = CNN_mod(num_features=3, num_classes=num_classes, learning_rate=learning_rate,
                               num_markers=num_markers, batch_size=batch_size, seq_len=seq_len)
        self.feat_mod = FEAT_mod(num_features=5, num_classes=num_classes, learning_rate=learning_rate,
                                 num_markers=num_markers, batch_size=batch_size, seq_len=seq_len)
        self.st_gcn = ST_GCN_simple(num_features=30, num_classes=num_classes, learning_rate=learning_rate,
                                    num_markers=num_markers, batch_size=batch_size, seq_len=seq_len)

        # Fully connected layers
        self.fc_main = torch.nn.Linear(7600, 2048)
        self.fc1 = torch.nn.Linear(2048, 800)
        self.fc2 = torch.nn.Linear(800, 400)
        self.fc3 = torch.nn.Linear(400, 64)

        self.fc4 = torch.nn.Linear(64, num_classes)

        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self,  x_graph, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, x_image, x_coord, x_feature, batch_ids):
        features_cnn_coord = self.cnn_coord(x_coord)
        print('shape RGB:', features_cnn_coord.shape)
        features_cnn_mod = self.cnn_mod(x_image)
        print('shape CNNim:', features_cnn_mod.shape)
        features_feat_mod = self.feat_mod(x_feature)
        print('shape featu:', features_feat_mod.shape)
        features_st_gcn = self.st_gcn(x_graph, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, batch_ids)
        print('shape st:', features_st_gcn.shape)
        combined_features = torch.cat([features_cnn_coord, features_cnn_mod, features_feat_mod, features_st_gcn], dim=1)
        print('shape comb:', combined_features.shape)
        x = F.leaky_relu(self.fc_main(combined_features))
        x = F.leaky_relu(self.fc1(x))
        x = F.dropout(x, p=0.2, training=self.training)
        x = F.leaky_relu(self.fc2(x))
        x = F.dropout(x, p=0.1, training=self.training)
        x = F.leaky_relu(self.fc3(x))
        x = self.fc4(x)
        return x

    def training_step(self, batch, batch_idx):
        x_graph, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, x_image, x_coord, x_feature, y, batch_ids = batch.x, batch.edge_index_spatial, batch.edge_weight_spatial, batch.edge_index_temporal, batch.edge_weight_temporal, batch.x_image, batch.x_coord, batch.x_feature, batch.y, batch.batch
        output = self(x_graph, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, x_image, x_coord, x_feature, batch_ids)
        # loss = F.nll_loss(output, y)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('Train', accuracy)
        self.train_confusion_matrix.update(preds, y)

        self.log('train_loss', loss)
        self.log('train_accuracy', accuracy, on_epoch=True, prog_bar=True)

        preds = torch.argmax(output, dim=1)
        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'train_reported_{name}' if name == 'accuracy' else f'train_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        x_graph, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, x_image, x_coord, x_feature, y, batch_ids = batch.x, batch.edge_index_spatial, batch.edge_weight_spatial, batch.edge_index_temporal, batch.edge_weight_temporal, batch.x_image, batch.x_coord, batch.x_feature, batch.y, batch.batch
        output = self(x_graph, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal,
                      x_image, x_coord, x_feature, batch_ids)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('VAL', accuracy)
        self.val_confusion_matrix.update(preds, y)

        self.log('val_loss', loss)
        self.log('val_accuracy', accuracy, on_epoch=True, prog_bar=True)

        preds = torch.argmax(output, dim=1)
        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'val_reported_{name}' if name == 'accuracy' else f'val_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'val_loss': loss}

    def on_train_epoch_end(self):
        cm = self.train_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/multMod/train_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.train_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def on_validation_epoch_end(self):
        cm = self.val_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/multMod/val_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.val_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        lr_scheduler = {
            'scheduler': torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10,
                                                                    verbose=True),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1,
            'strict': True,
        }
        return {'optimizer': optimizer, 'lr_scheduler': lr_scheduler}

