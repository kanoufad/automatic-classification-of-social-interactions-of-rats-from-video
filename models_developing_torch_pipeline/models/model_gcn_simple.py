import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv
from torch_geometric.nn import GATConv
from pytorch_lightning import LightningModule
from torch_geometric.nn import global_mean_pool
from ignite.metrics import Accuracy, Precision, Recall
from ignite.utils import to_onehot
from sklearn.metrics import classification_report
from torch.utils.tensorboard import SummaryWriter
import torchmetrics
import gc
import matplotlib.pyplot as plt
import numpy as np


def flatten_dict(d, parent_key='', sep='_'):
    items = {}
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.update(flatten_dict(v, new_key, sep=sep))
        else:
            items[new_key] = v
    return items


def plot_confusion_matrix(cm, filename):
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.colormaps['Blues'])
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           ylabel='True label',
           xlabel='Predicted label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    plt.savefig(filename)
    plt.close(fig)


class ST_GCN_simple(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(ST_GCN_simple, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len
        self.train_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')
        self.val_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')

        self.spatial_conv1 = GCNConv(num_features, 64)
        self.spatial_conv2 = GCNConv(64, 128)

        self.temporal_conv1 = GCNConv(num_features, 64)
        self.temporal_conv2 = GCNConv(64, 128)

        # Fully connected layers
        self.fc1 = torch.nn.Linear(256, 128)
        self.fc2 = torch.nn.Linear(128, 64)

        self.fc3 = torch.nn.Linear(64, num_classes)

        self.adaptive_avg_pool = nn.AdaptiveAvgPool1d(256)
        self.lstm_comm = nn.LSTM(self.num_markers * 2 * 128, 256, 1, batch_first=True)
        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, batch_ids):
        # Spatial stream

        x_spatial = F.relu(self.spatial_conv1(x, edge_index_spatial))
        # print('gcnn spat1', x_spatial.shape)
        x_spatial = F.dropout(x_spatial, p=0.2, training=self.training)
        x_spatial = F.relu(self.spatial_conv2(x_spatial, edge_index_spatial))
        # print(x_spatial.shape)
        out_spatial = global_mean_pool(x_spatial, batch_ids)

        # # Temporal stream
        x_temporal = F.relu(self.temporal_conv1(x, edge_index_temporal))
        x_temporal = F.dropout(x_temporal, p=0.2, training=self.training)
        x_temporal = F.relu(self.temporal_conv2(x_temporal, edge_index_temporal))

        x_temp_global = global_mean_pool(x_temporal, batch_ids)

        x = torch.cat([out_spatial, x_temp_global], dim=1)

        # Fully connected layers
        x = F.relu(self.fc1(x))
        x = F.dropout(x, p=0.2, training=self.training)
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def training_step(self, batch, batch_idx):
        x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, y, batch_ids = batch.x, batch.edge_index_spatial, batch.edge_weight_spatial, batch.edge_index_temporal, batch.edge_weight_temporal, batch.y, batch.batch
        output = self(x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, batch_ids)
        # loss = F.nll_loss(output, y)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('Train', accuracy)
        self.train_confusion_matrix.update(preds, y)

        self.log('train_loss', loss)
        self.log('train_accuracy', accuracy, on_epoch=True, prog_bar=True)

        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'train_reported_{name}' if name == 'accuracy' else f'train_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, y, batch_ids = batch.x, batch.edge_index_spatial, batch.edge_weight_spatial, batch.edge_index_temporal, batch.edge_weight_temporal, batch.y, batch.batch
        output = self(x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal, batch_ids)
        # loss = F.nll_loss(output, y)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('VAL', accuracy)
        self.log('val_loss', loss)
        self.log('val_accuracy', accuracy, on_epoch=True, prog_bar=True)

        self.val_confusion_matrix.update(preds, y)

        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'val_reported_{name}' if name == 'accuracy' else f'val_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'val_loss': loss}

    def on_train_epoch_end(self):
        cm = self.train_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/model_gcn_p24_simple/train_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.train_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def on_validation_epoch_end(self):
        cm = self.val_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/model_gcn_p24_simple/val_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.val_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        lr_scheduler = {
            'scheduler': torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10,
                                                                    verbose=True),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1,
            'strict': True,
        }
        return {'optimizer': optimizer, 'lr_scheduler': lr_scheduler}

