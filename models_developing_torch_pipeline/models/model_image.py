import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv
from pytorch_lightning import LightningModule
from torch_geometric.nn import global_mean_pool
from ignite.metrics import Accuracy, Precision, Recall
from ignite.utils import to_onehot
from sklearn.metrics import classification_report
from torch.utils.tensorboard import SummaryWriter
from torchvision import models
import torchmetrics
import gc
import matplotlib.pyplot as plt
import numpy as np


def flatten_dict(d, parent_key='', sep='_'):
    items = {}
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.update(flatten_dict(v, new_key, sep=sep))
        else:
            items[new_key] = v
    return items


def plot_confusion_matrix(cm, filename):
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.colormaps['Blues'])
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           ylabel='True label',
           xlabel='Predicted label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    plt.savefig(filename)
    plt.close(fig)


class CNN_mod(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(CNN_mod, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len
        self.train_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')
        self.val_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=num_classes, task='multiclass')

        # Image network
        self.resnet = models.resnet18(pretrained=True)
        self.resnet.fc = nn.Identity()
        self.image_lstm = nn.LSTM(4*512, 512, batch_first=True)

        self.fc2 = torch.nn.Linear(512, 128)

        self.fc3 = torch.nn.Linear(128, 64)
        self.fc4 = torch.nn.Linear(64, num_classes)

        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x):
        images = x
        # print('shape model:', images.shape)
        batch_size_four_times, seq_length, c, h, w = images.shape
        images = images.view(batch_size_four_times * seq_length, c, h, w)
        x = self.resnet(images)
        # print(x.shape)
        x = x.view(batch_size_four_times, seq_length, -1)
        batch_size = batch_size_four_times // 4
        x = x.view(batch_size, 4, seq_length, -1)
        # print(x.shape)
        x = x.permute(0, 2, 1, 3)
        # print(x.shape)
        x = x.reshape(batch_size, seq_length, -1)
        # print(x.shape)
        x, _ = self.image_lstm(x)
        x = torch.mean(x, dim=1)
        x = F.dropout(x, p=0.2, training=self.training)
        x = F.leaky_relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)

        return x

    def training_step(self, batch, batch_idx):
        x = batch.x
        y = batch.y
        output = self(x)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('Train', accuracy)

        self.log('train_loss', loss)
        self.log('train_accuracy', accuracy, on_epoch=True, prog_bar=True)
        self.train_confusion_matrix.update(preds, y)

        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'train_reported_{name}' if name == 'accuracy' else f'train_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        x = batch.x
        y = batch.y
        output = self(x)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('VAL', accuracy)
        self.log('val_loss', loss)
        self.log('val_accuracy', accuracy, on_epoch=True, prog_bar=True)
        self.val_confusion_matrix.update(preds, y)

        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'val_reported_{name}' if name == 'accuracy' else f'val_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'val_loss': loss}

    def on_train_epoch_end(self):
        cm = self.train_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/model_image_normal_final/train_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.train_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def on_validation_epoch_end(self):
        cm = self.val_confusion_matrix.compute().cpu().numpy()

        filename = f"E:/data/model_training_results/conf_mat/model_image_normal_final/val_confusion_matrix_epoch_{self.current_epoch}.png"
        plot_confusion_matrix(cm, filename)
        self.val_confusion_matrix.reset()

        gc.collect()
        torch.cuda.empty_cache()

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        lr_scheduler = {
            'scheduler': torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10,
                                                                    verbose=True),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1,
            'strict': True,
        }
        return {'optimizer': optimizer, 'lr_scheduler': lr_scheduler}




