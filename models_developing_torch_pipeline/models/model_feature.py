import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv
from pytorch_lightning import LightningModule
from torch_geometric.nn import global_mean_pool
from ignite.metrics import Accuracy, Precision, Recall
from ignite.utils import to_onehot
from sklearn.metrics import classification_report
from torch.utils.tensorboard import SummaryWriter
from torchvision import models


def flatten_dict(d, parent_key='', sep='_'):
    items = {}
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.update(flatten_dict(v, new_key, sep=sep))
        else:
            items[new_key] = v
    return items


class FEAT_mod(LightningModule):
    def __init__(self, num_features, num_classes, learning_rate, num_markers, batch_size, seq_len):
        super(FEAT_mod, self).__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.y_true_train = []
        self.y_pred_train = []
        self.y_true_val = []
        self.y_pred_val = []
        self.writer = SummaryWriter()
        self.num_markers = num_markers
        self.batch_size = batch_size
        self.seq_len = seq_len

        self.convs = nn.ModuleList(
            [nn.Conv1d(in_channels=1, out_channels=16, kernel_size=3, padding=1) for _ in range(num_features)])

        # Fully connected layers
        self.fc1 = nn.Linear(16 * num_features * seq_len, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, num_classes)

        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x):
        # x shape: [batch_size, seq_len, num_features]
        x = x.float()
        # print(x.shape)
        x = x.view(self.batch_size, self.seq_len, -1)
        # print(x.shape)

        x = x.permute(0, 2, 1).unsqueeze(2)  # dhape: [batch_size, num_features, 1, seq_len]

        conv_outs = [F.relu(conv(x[:, i, :, :])) for i, conv in enumerate(self.convs)]
        x = torch.cat(conv_outs, dim=1)

        x = torch.flatten(x, start_dim=1)

        x = F.relu(self.fc1(x))
        x = F.dropout(x, p=0.2, training=self.training)
        x = F.relu(self.fc2(x))
        x = self.fc3(x)

        return x

    def training_step(self, batch, batch_idx):
        x = batch.x
        y = batch.y
        output = self(x)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('Train', accuracy)

        self.log('train_loss', loss)
        self.log('train_accuracy', accuracy, on_epoch=True, prog_bar=True)

        preds = torch.argmax(output, dim=1)
        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'train_reported_{name}' if name == 'accuracy' else f'train_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        x = batch.x
        y = batch.y
        output = self(x)
        loss = self.loss_fn(output, y)

        preds = torch.argmax(output, dim=1)
        correct = (preds == y).float()
        accuracy = correct.sum() / len(correct)
        print('VAL', accuracy)
        self.log('val_loss', loss)
        self.log('val_accuracy', accuracy, on_epoch=True, prog_bar=True)

        preds = torch.argmax(output, dim=1)
        report = classification_report(y.cpu().numpy(), preds.cpu().numpy(), output_dict=True, zero_division=0)
        flat_report = flatten_dict(report)

        for name, value in flat_report.items():
            metric_name = f'val_reported_{name}' if name == 'accuracy' else f'val_{name}'
            self.log(metric_name, value, on_epoch=True, prog_bar=True)

        return {'val_loss': loss}

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        lr_scheduler = {
            'scheduler': torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10,
                                                                    verbose=True),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1,
            'strict': True,
        }
        return {'optimizer': optimizer, 'lr_scheduler': lr_scheduler}


