from data_loader import RatBehaviorDataModule
import torch
from models.model_gat import TwoStreamGNN
from models.model_image import CNN_mod
from models.model_gcn_simple import ST_GCN_simple
from models.model_gcn_simple_weighted import ST_GCN_simple_weighted
from models.model_gcn_lstm import ST_GCN_LSTM
from models.model_coord_image import CNN_coord
from models.model_cuboid import CNN_cuboid
from models.model_feature import FEAT_mod
from models.model_active_gcn_simple import Active_GCN_simple_weighted
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger, CSVLogger
from pytorch_lightning.callbacks import ModelCheckpoint
from models.model_AS_gcn import ActGCN
from models.model_concat1 import MultiMod
from models.model_concat2 import MultiMod_late
from models.model_1d_conv import mod1Conv
from models.model_coord_image_pivot import CNN_coord_pivot
from models.model_concat3 import MultiMod_later
from models.model_combined_v1_pretrained import CombinedModel
from models.model_gcn_series import ST_GCN_series
import torch.multiprocessing as mp


import os

if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
    mp.set_start_method('spawn', force=True)
    TORCH_USE_CUDA_DSA = 1

    datasets_own = [
        r'E:\data\wt5_wt6_10m',
        r'E:\data\wt1_wt3_shuffle4',
        r'E:\data\ad2_ad3_10m_train_dlc',
        r'E:\data\ad2_wt1_10m',
        r'E:\data\ad4_wt3_10m',
        r'E:\data\passive_int_video\cut_videos',
        r'E:\data\wt1_wt2_shuffle4',
        r'E:\data\ad6_wt6_shuffle4',
        r'E:\data\ad1_wt1_shuffle4',
        r'E:\data\ad5_ad6_10m',
        r'E:\data\ad5_ad8_10m',
        r'E:\data\wt1_wt4_10m_train_dlc',
        r'E:\data\ad3_wt4_10m'
    ]

    datasets_p24 = [
        'E:/data/PAIR-R24M-Dataset/20210122_Recording_SR1_SR2_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210125_Recording_SR1_SR3_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210128_Recording_SR2+SR3redH',
        'E:/data/PAIR-R24M-Dataset/20210129_Recording_SR1_SR4_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210201_Recording_SR2_SR4red_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210201_Recording_SR4red_SR5_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210202_Recording_SR2_SR5red_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210202_Recording_SR3_SR5red_social_wvid',
        # 'E:/data/PAIR-R24M-Dataset/20210203_Recording_SR1_SR2_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210203_Recording_SR1_SR3_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210203_Recording_SR1_SR5_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210203_Recording_SR2red_SR5_social_wvid',
        'E:/data/PAIR-R24M-Dataset/20210203_Recording_SR2red_SR3_social_wvid',
        # 'E:/data/PAIR-R24M-Dataset/20210202_Recording_SR3_SR4red_social_wvid',
        # 'E:/data/PAIR-R24M-Dataset/20210201_Recording_SR1_SR5_social_wvid',
        # 'E:/data/PAIR-R24M-Dataset/20210203_Recording_SR1_SR4_social_wvid'
    ]
    start_time = 5  # in seconds
    gap_time = 20  # in seconds
    fps = 30  # frames per second
    seq_len = 15

    datasets = datasets_p24
    # datasets = datasets_own

    if datasets == datasets_p24:
        batch_size = 12
        num_markers = 12
        NUM_CLASSES = 4
        dat_p24 = True
    else:
        batch_size = 48
        # batch_size = 8
        num_markers = 9
        NUM_CLASSES = 8
        dat_p24 = False

    plot_data = False

    excluded_labels = [1001]
    # graph_type = 'dynamic'
    # graph_type = 'simple'
    graph_type = 'fully_connected'
    # graph_type = 'simple_weighted'
    # graph_type = 'active'
    # graph_type = 'rgb'
    # graph_type = 'rgb'
    model_type = 'coord'  # 'coord'
    # model_type = 'image'
    # model_type = 'cuboid'
    # model_type = 'features'
    # model_type = 'all'
    # model_type = 'all_late'
    # model_type = 'iniciator'
    if model_type == 'iniciator':
        NUM_CLASSES = 2
        batch_size = 11

    data_module = RatBehaviorDataModule(datasets, start_time, gap_time, fps, batch_size, plot_data, num_markers,
                                        graph_type, excluded_labels, model_type, seq_len, p24=dat_p24)
    data_module.setup()

    model = 0
    if model_type == 'coord' and graph_type == 'dynamic':
        model = TwoStreamGNN(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                             batch_size=batch_size, seq_len=seq_len)
    if model_type == 'coord' and (graph_type == 'simple' or graph_type == 'fully_connected'):
        # model = ST_GCN_simple(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
        #                       batch_size=batch_size, seq_len=seq_len)
        # model = mod1Conv(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
        #                  batch_size=batch_size, seq_len=seq_len)
        # print('gcn right')
        model = ST_GCN_series(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                             batch_size=batch_size, seq_len=seq_len)

    if model_type == 'coord' and (graph_type == 'simple_weighted' or graph_type == 'active' or graph_type == 'rgb'):
        # model = ST_GCN_simple_weighted(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001,
        #                                num_markers=num_markers,
        #                                batch_size=batch_size, seq_len=seq_len)
        # model = ActGCN(num_features=6, num_classes=NUM_CLASSES, learning_rate=0.001,
        #                num_markers=num_markers,
        #                batch_size=batch_size, seq_len=seq_len)
        model = CNN_coord(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001,
                          num_markers=num_markers,
                          batch_size=batch_size, seq_len=seq_len)
    if model_type == 'coord' and graph_type == 'rgb2':
        model = CNN_coord_pivot(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001,
                                num_markers=num_markers,
                                batch_size=batch_size, seq_len=seq_len)
    elif model_type == 'image':
        model = CNN_mod(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                        batch_size=batch_size, seq_len=seq_len)
    elif model_type == 'cuboid':
        model = CNN_cuboid(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                           batch_size=batch_size, seq_len=seq_len-1)
    elif model_type == 'features':
        model = FEAT_mod(num_features=5, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                         batch_size=batch_size, seq_len=seq_len)
    if model_type == 'all':
        model = MultiMod_late(num_features=5, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                              batch_size=batch_size, seq_len=seq_len)
    if model_type == 'all_late':
        # model = MultiMod_later(num_features=5, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
        #                        batch_size=batch_size, seq_len=seq_len)
        model = CombinedModel(num_features=5, num_classes=NUM_CLASSES, learning_rate=0.001, num_markers=num_markers,
                              batch_size=batch_size, seq_len=seq_len)
    if model_type == 'iniciator':
        print('model here inic')
        model = CNN_coord(num_features=3, num_classes=NUM_CLASSES, learning_rate=0.001,
                          num_markers=num_markers,
                          batch_size=batch_size, seq_len=seq_len)
    model = model.to(device)
    # name_log = 'model_image_normal_final'
    # name_log = 'model_combined_v1'
    name_log = 'model_gcn_p24_simple_right'
    logger = TensorBoardLogger("E:/data/model_training_results/tb_logs", name=name_log)
    csv_logger = CSVLogger("E:/data/model_training_results/csv_logs", name=name_log)

    checkpoint_callback = ModelCheckpoint(
        # dirpath='E:/model_checkpoints/image_model_normal',
        # dirpath='E:/model_checkpoints/model_coord_rgb_nonorm',
        dirpath='E:/model_checkpoints/model_combined_v1',

        filename='{epoch}-{val_accuracy:.2f}',
        save_top_k=5,
        verbose=True,
        monitor='val_loss',
        mode='min'
    )
    # tensorboard --logdir C:\Users\fgu032lab034\PycharmProjects\behav_model_new\tensorboard_logs

    train_loader = data_module.train_dataloader()
    print(len(train_loader))

    val_loader = data_module.val_dataloader()
    print(len(val_loader))

    trainer = Trainer(max_epochs=30, accelerator="gpu", devices=1, logger=[logger, csv_logger], log_every_n_steps=5,
                      callbacks=[checkpoint_callback])

    # Train model
    trainer.fit(model, train_loader, val_loader)
