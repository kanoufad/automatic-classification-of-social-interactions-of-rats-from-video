import torch
from models.model_combined_v1_pretrained import CombinedModel
from models.model_coord_image import CNN_coord
import numpy as np
from features import build_rgb_sequence
import os
from PIL import Image
from torchvision.transforms import Compose, Resize, ToTensor, Normalize, InterpolationMode
import sys
from scipy.io import loadmat


DEFAULT_NUM_FEATURES = 3
DEFAULT_NUM_CLASSES = 8
DEFAULT_LEARNING_RATE = 0.001
DEFAULT_NUM_MARKERS = 9
DEFAULT_BATCH_SIZE = 1
DEFAULT_SEQ_LEN = 15
DEFAULT_NUM_CLASSES_INIT = 2


def load_model(checkpoint_path, num_features=DEFAULT_NUM_FEATURES, num_classes=DEFAULT_NUM_CLASSES,
               learning_rate=DEFAULT_LEARNING_RATE, num_markers=DEFAULT_NUM_MARKERS,
               batch_size=DEFAULT_BATCH_SIZE, seq_len=DEFAULT_SEQ_LEN):
    model = CombinedModel(num_features=num_features, num_classes=num_classes,
                          learning_rate=learning_rate, num_markers=num_markers,
                          batch_size=batch_size, seq_len=seq_len)
    model.load_state_dict(torch.load(checkpoint_path)['state_dict'])
    model.eval()
    return model


def load_model_init(checkpoint_path, num_features=DEFAULT_NUM_FEATURES, num_classes=DEFAULT_NUM_CLASSES_INIT,
                    learning_rate=DEFAULT_LEARNING_RATE, num_markers=DEFAULT_NUM_MARKERS,
                    batch_size=DEFAULT_BATCH_SIZE, seq_len=DEFAULT_SEQ_LEN):
    model = CNN_coord(num_features=num_features, num_classes=num_classes,
                      learning_rate=learning_rate, num_markers=num_markers,
                      batch_size=batch_size, seq_len=seq_len)
    model.load_state_dict(torch.load(checkpoint_path)['state_dict'])
    model.eval()
    return model


def _preprocess_image(img_path):
    img = Image.open(img_path).convert("RGB")

    img_transforms = Compose([
        Resize((112, 112), interpolation=InterpolationMode.BILINEAR),
        ToTensor(),
        Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])
    img_tensor = img_transforms(img)

    return img_tensor


def _load_image(frame_idx, cam_idx, dataset_path):
    images_path = os.path.join(dataset_path, 'model_data', 'images')
    image_path = os.path.join(images_path, f'{frame_idx+1}_{cam_idx}.jpg')
    img_x = _preprocess_image(image_path)
    return img_x


def _process_image(start_frame, end_frame, dataset_path):
    views_seq = []
    for view in range(1, 5):
        view_seq = []

        for im in range(start_frame, end_frame+1):
            view_seq_image = _load_image(im, view, dataset_path)
            view_seq.append(view_seq_image)

        views_seq.append(torch.stack(view_seq))

    all_views_seq = torch.stack(views_seq)

    return all_views_seq


def predict_sequence(model, dataset_path, start_frame, end_frame, coord_mat):
    device = next(model.parameters()).device
    x_images = _process_image(start_frame, end_frame, dataset_path).to(device)
    x_rgb = torch.tensor(np.array(build_rgb_sequence(coord_mat))).to(device)

    if torch.isnan(x_rgb).any():
        return np.array([1004])

    with torch.no_grad():
        prediction = model(x_images, x_rgb, batch_ids=None)

    predicted_class = torch.argmax(prediction, dim=1)
    return predicted_class.cpu().numpy()


def predict_sequence_init(model, dataset_path, start_frame, end_frame, coord_mat):
    device = next(model.parameters()).device
    x_rgb = torch.tensor(np.array(build_rgb_sequence(coord_mat))).to(device)

    if torch.isnan(x_rgb).any():
        return np.array([1004])

    with torch.no_grad():
        prediction = model(x_rgb)

    predicted_class = torch.argmax(prediction, dim=1)
    return predicted_class.cpu().numpy()


def rotation_matrix(rot_axis, theta):
    rot_axis = rot_axis / np.linalg.norm(rot_axis)
    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)
    minus_cos = 1 - cos_theta
    x, y, z = rot_axis

    rot_matrix = np.array([
        [cos_theta + x ** 2 * minus_cos, x * y * minus_cos - z * sin_theta,
         x * z * minus_cos + y * sin_theta],
        [y * x * minus_cos + z * sin_theta, cos_theta + y ** 2 * minus_cos,
         y * z * minus_cos - x * sin_theta],
        [z * x * minus_cos - y * sin_theta, z * y * minus_cos + x * sin_theta,
         cos_theta + z ** 2 * minus_cos]
    ])

    return rot_matrix


# Plane
arena_corners = np.array([
    [-311.517677765096, 40.2822930457915, 949.255098395399],
    [166.829668699406, 36.2070176477812, 979.625206494983],
    [193.732915175345, 313.440593621476, 582.513110520185],
    [-274.070068351027, 315.574241893707, 543.672604868204]
])

v1 = arena_corners[1] - arena_corners[0]
v2 = arena_corners[3] - arena_corners[0]

# Normal to it
normal = np.cross(v1, v2)
normal = -normal / np.linalg.norm(normal)

# Rotation
rotation_axis = np.cross(normal, [0, 0, 1])
rotation_axis = rotation_axis / np.linalg.norm(rotation_axis)
angle = np.arccos(np.dot(normal, [0, 0, 1]))

rot_mat = rotation_matrix(rotation_axis, angle)
aligned_corners = np.dot(rot_mat, arena_corners.T).T


def create_coordinates_matrix(coordinates_rat, num_markers):
    sequence_length = 15
    coord_mat = np.zeros((sequence_length, num_markers, 3))
    # print(coordinates_rat.shape)
    for frame in range(0, 15):
        for marker in range(0, num_markers):
            x, y, z = coordinates_rat[marker, :, frame]
            # print('coords:', x,y,z)
            coord_mat[frame, marker, :] = [x, y, z]

    return torch.tensor(coord_mat, dtype=torch.float32)


def align_plane_and_rotate(coord_mat, rot_mat, aligned_corners):

    for i in range(coord_mat.shape[0]):
        coord_mat_np = coord_mat[i, :, :].numpy()
        rotated_coords_np = np.dot(rot_mat, coord_mat_np.T).T
        coord_mat[i, :, :] = torch.from_numpy(rotated_coords_np).type_as(coord_mat)

    return coord_mat, aligned_corners


def build_coord_mat(coord_mat_rat1, coord_mat_rat2):
    return torch.cat((coord_mat_rat1, coord_mat_rat2), dim=1)  # Shape: [15, 18, 3]


def main():
    if len(sys.argv) != 6:
        print("Usage: python predictions.py <dataset_path> <start_index> <end_index> <rat1_coordinates_file> <rat2_coordinates_file>")
        sys.exit(1)

    dataset_path = sys.argv[1]
    start_index = int(sys.argv[2])  # Time in seconds
    end_index = int(sys.argv[3])    # Time in seconds
    rat1_coordinates = loadmat(sys.argv[4])['seq_r1']
    rat2_coordinates = loadmat(sys.argv[5])['seq_r2']

    frame_rate = 30  # Frames per second
    sequence_length = 15  # Length of the sequence in frames

    start_frame = start_index * frame_rate
    end_frame = end_index * frame_rate

    model_path = "E:/model_checkpoints/model_combined_v1/val_accuracy=0.86-v1.ckpt"
    model_oi_path = "E:/model_checkpoints/model_coord_iniciator_approach/val_accuracy=0.98.ckpt"
    model_ap_path = "E:/model_checkpoints/model_coord_iniciator_approach_actual/val_accuracy=1.00.ckpt"
    model_dt_path = "E:/model_checkpoints/model_coord_iniciator_dettach/val_accuracy=1.00.ckpt"
    model_mt_path = "E:/model_checkpoints/model_coord_iniciator_mounting2/val_accuracy=1.00.ckpt"
    print('Dataset: ', dataset_path)
    model = load_model(model_path)
    model.cuda()
    print('loaded ModelMulti')
    model_oi = load_model_init(model_oi_path).cuda()
    model_ap = load_model_init(model_ap_path).cuda()
    model_dt = load_model_init(model_dt_path).cuda()
    model_mt = load_model_init(model_mt_path).cuda()
    print('loaded initiator Models')
    num_markers = 9

    predictions_folder = os.path.join(dataset_path, 'model_data', 'action_predicts')
    initiator_predictions_folder = os.path.join(dataset_path, 'model_data', 'action_predicts_initiator')
    os.makedirs(predictions_folder, exist_ok=True)
    os.makedirs(initiator_predictions_folder, exist_ok=True)
    print('predicting...')
    for current_start in range(0, 600, sequence_length):
        current_end = current_start + sequence_length - 1

        coord_mat_rat1 = create_coordinates_matrix(rat1_coordinates[:, :, current_start:current_end + 1], num_markers)
        coord_mat_rat2 = create_coordinates_matrix(rat2_coordinates[:, :, current_start:current_end + 1], num_markers)

        coord_mat, aligned_corners2 = align_plane_and_rotate(build_coord_mat(coord_mat_rat1, coord_mat_rat2),
                                                             rot_mat, aligned_corners)

        predicted_sequence = predict_sequence(model, dataset_path, start_frame + current_start,
                                              start_frame + current_end, coord_mat)

        output_file = os.path.join(predictions_folder, f'{start_index}_{end_index}_actions_predictions.txt')

        with open(output_file, 'a') as f:
            for pred in predicted_sequence:
                f.write(f"{pred}\n")

        initiator_output_file = os.path.join(initiator_predictions_folder, f'{start_index}_{end_index}_actions_predictions_initiator.txt')

        with open(initiator_output_file, 'a') as f_init:
            for pred in predicted_sequence:
                initiator_pred = 1003
                if pred == 2:  # Olfactory Exploration
                    initiator_pred = predict_sequence_init(model_oi, dataset_path, start_frame + current_start,
                                                           start_frame + current_end, coord_mat)
                elif pred == 3:  # Approach
                    initiator_pred = predict_sequence_init(model_ap, dataset_path, start_frame + current_start,
                                                           start_frame + current_end, coord_mat)
                elif pred == 4:  # Dettach
                    initiator_pred = predict_sequence_init(model_dt, dataset_path, start_frame + current_start,
                                                           start_frame + current_end, coord_mat)
                elif pred == 5:  # Mounting
                    initiator_pred = predict_sequence_init(model_mt, dataset_path, start_frame + current_start,
                                                           start_frame + current_end, coord_mat)
                if initiator_pred == 1003 or initiator_pred == 1004:
                    f_init.write(f"{initiator_pred}\n")
                else:
                    f_init.write(f"{initiator_pred[0]}\n")

        print("Predicted Sequence for frames", current_start, "to", current_end, ":", predicted_sequence)


if __name__ == "__main__":
    main()