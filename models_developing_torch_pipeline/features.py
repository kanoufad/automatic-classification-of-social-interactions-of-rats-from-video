import torch
import numpy as np
import random
import os
import matplotlib.pyplot as plt
import math
from PIL import Image
from torchvision.transforms import Compose, Resize, ToTensor, Normalize, InterpolationMode
from torchvision.transforms import functional as F


def create_coordinates_matrix_p24(coordinates_rat_df, sequence_length, num_markers):
    coordinates_data_np = coordinates_rat_df.to_numpy()
    coord_mat = coordinates_data_np.reshape(sequence_length, num_markers, 3, order='C')
    return torch.tensor(coord_mat, dtype=torch.float32)


def create_coordinates_matrix(coordinates_df, start_idx, end_idx, num_markers):
    sequence_length = end_idx - start_idx + 1
    coord_mat = np.zeros((sequence_length, num_markers, 3))

    for frame in range(start_idx, end_idx + 1):
        frame_coordinates = coordinates_df[coordinates_df.iloc[:, 0] == frame]
        for marker in range(1, num_markers + 1):
            marker_coordinates = frame_coordinates[frame_coordinates.iloc[:, 1] == marker]
            x, y, z = marker_coordinates.iloc[:, 3].values
            coord_mat[frame - start_idx, marker - 1, :] = [x, y, z]

    return torch.tensor(coord_mat, dtype=torch.float32)


def create_features_matrix(features_df, start_idx, end_idx, num_features):
    sequence_length = end_idx - start_idx + 1
    feature_mat = np.zeros((sequence_length, num_features))

    for frame in range(start_idx-1, end_idx):
        frame_features = features_df.iloc[frame, :]

        if frame_features.empty or frame_features.isnull().values.any():
            continue

        feature_mat[frame - start_idx, :] = frame_features.values.squeeze()

    return feature_mat


def build_fully_connected_graph(coord_mat_rat1, coord_mat_rat2, num_markers=9):

    sequence_length = coord_mat_rat1.size(0)
    total_markers = 2 * num_markers
    graph = torch.zeros((sequence_length, total_markers, total_markers, 3))

    combined_coordinates = torch.cat((coord_mat_rat1, coord_mat_rat2), dim=1)

    for t in range(sequence_length):
        for i in range(total_markers):
            for j in range(total_markers):
                if i == j:
                    continue
                graph[t, i, j, :] = torch.norm(
                    combined_coordinates[t, i, :] - combined_coordinates[t, j, :], dim=-1
                )

    return graph


def to_polar_coords(x, y, z, ref_x, ref_y, ref_z):
    dx, dy, dz = x - ref_x, y - ref_y, z - ref_z
    r = np.sqrt(dx**2 + dy**2 + dz**2)
    theta = np.arccos(dz/r) if r != 0 else 0
    fi = np.arctan2(dy, dx)
    return r, theta, fi


def adjust_coord_matrix(coord_mat, arena_corners):
    arena_corners_tensor = torch.tensor(arena_corners, dtype=coord_mat.dtype, device=coord_mat.device)

    minX = torch.min(arena_corners_tensor[:, 0])
    maxX = torch.max(arena_corners_tensor[:, 0])
    minY = torch.min(arena_corners_tensor[:, 1])
    maxY = torch.max(arena_corners_tensor[:, 1])
    minZ = torch.min(arena_corners_tensor[:, 2])
    maxZ = torch.max(arena_corners_tensor[:, 2])

    centerX = (minX + maxX) / 2
    centerY = (minY + maxY) / 2
    centerZ = (minZ + maxZ) / 2
    arena_center = torch.tensor([centerX, centerY, centerZ], dtype=coord_mat.dtype, device=coord_mat.device)
    # print(coord_mat.shape)
    sequence_length, num_markers, _ = coord_mat.shape
    extended_mat = torch.zeros((sequence_length, num_markers, 3))

    for frame in range(sequence_length):
        for marker in range(num_markers):
            x, y, z = coord_mat[frame, marker, :].numpy()
            centered_x = x - arena_center[0]
            centered_y = y - arena_center[1]
            centered_z = z - arena_center[2]

            r, theta, fi = to_polar_coords(centered_x, centered_y, centered_z, 0, 0, 0)
            extended_mat[frame, marker, :] = torch.tensor([r, theta, fi])
    # print('EX', extended_mat.shape)
    return extended_mat


def build_pyg_graph_temporal(coord_mat, graph_option):

    x = coord_mat.view(-1, 3)

    num_nodes = coord_mat.shape[1]
    num_sequences = coord_mat.shape[0]

    edge_index_list_spatial = []
    edge_index_list_temporal = []

    spatial_connections = [
        (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8),
        (9, 10), (9, 11), (9, 12), (10, 12), (11, 12), (12, 13), (13, 14), (14, 15), (15, 16), (16, 17)
    ]
    if graph_option == 'simple' or graph_option == 'active':
        for seq in range(num_sequences):
            for (i, j) in spatial_connections:
                edge_index_list_spatial.append((seq * num_nodes + i, seq * num_nodes + j))
                edge_index_list_spatial.append((seq * num_nodes + j, seq * num_nodes + i))

        for seq in range(1, num_sequences):
            for i in range(num_nodes):
                edge_index_list_temporal.append(((seq - 1) * num_nodes + i, seq * num_nodes + i))

        edge_index_spatial = torch.tensor(edge_index_list_spatial, dtype=torch.long).t().contiguous()
        edge_index_temporal = torch.tensor(edge_index_list_temporal, dtype=torch.long).t().contiguous()
        edge_weight_spatial = torch.ones(edge_index_spatial.size(1), dtype=torch.float)
        edge_weight_temporal = torch.ones(edge_index_temporal.size(1), dtype=torch.float)

        return x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal

    if graph_option == 'fully_connected':
        for seq in range(num_sequences):
            for i in range(num_nodes):
                for j in range(num_nodes):
                    if i != j:
                        edge_index_list_spatial.append((seq * num_nodes + i, seq * num_nodes + j))
                        edge_index_list_spatial.append((seq * num_nodes + j, seq * num_nodes + i))

        for seq in range(1, num_sequences):
            for i in range(num_nodes):
                edge_index_list_temporal.append(((seq - 1) * num_nodes + i, seq * num_nodes + i))

        edge_index_spatial = torch.tensor(edge_index_list_spatial, dtype=torch.long).t().contiguous()
        edge_index_temporal = torch.tensor(edge_index_list_temporal, dtype=torch.long).t().contiguous()
        edge_weight_spatial = torch.ones(edge_index_spatial.size(1), dtype=torch.float)
        edge_weight_temporal = torch.ones(edge_index_temporal.size(1), dtype=torch.float)

        return x, edge_index_spatial, edge_weight_spatial, edge_index_temporal, edge_weight_temporal


def build_rgb_sequence(sequence_matrix):
    orders = [
        [1, 10, 1, 11, 1, 12, 1, 13, 1, 14, 1, 15, 1, 16, 1, 17, 1, 18],
        [5, 10, 5, 11, 5, 12, 5, 13, 5, 14, 5, 15, 5, 16, 5, 17, 5, 18],
        [9, 10, 9, 11, 9, 12, 9, 13, 9, 14, 9, 15, 9, 16, 9, 17, 9, 18],
        [10, 1, 10, 2, 10, 3, 10, 4, 10, 5, 10, 6, 10, 7, 10, 8, 10, 9],
        [14, 1, 14, 2, 14, 3, 14, 4, 14, 5, 14, 6, 14, 7, 14, 8, 14, 9],
        [18, 1, 18, 2, 18, 3, 18, 4, 18, 5, 18, 6, 18, 7, 18, 8, 18, 9]
    ]
    num_steps = sequence_matrix.shape[0]
    stacked_images = []

    for order in orders:
        rgb_images = []

        for t in range(num_steps):
            coords = sequence_matrix[t, :, :]
            distances = []
            azimuthal_angles = []
            polar_angles = []

            for idx in range(0, len(order), 2):
                i = order[idx] - 1
                j = order[idx + 1] - 1
                distance, azimuthal_angle, polar_angle = calculate_features(coords[i], coords[j])

                distances.append(distance)
                azimuthal_angles.append(azimuthal_angle)
                polar_angles.append(polar_angle)

            max_distance = max(distances) if distances else 1
            distances_norm = [d / max_distance for d in distances]
            azimuthal_angles_norm = [(np.cos(np.deg2rad(a)) + 1) / 2 for a in azimuthal_angles]
            polar_angles_norm = [a / 180 for a in polar_angles]

            rgb_image_row = np.stack([distances_norm, azimuthal_angles_norm, polar_angles_norm], axis=-1)
            rgb_images.append(rgb_image_row)

        stacked_images.append(np.stack(rgb_images, axis=0))

    concatenated_images = np.concatenate(stacked_images, axis=1)
    return concatenated_images


def build_rgb_sequence_2(coord_mat, arena_corners):
    arena_corners_tensor = torch.tensor(arena_corners, dtype=coord_mat.dtype, device=coord_mat.device)

    minX = torch.min(arena_corners_tensor[:, 0])
    maxX = torch.max(arena_corners_tensor[:, 0])
    minY = torch.min(arena_corners_tensor[:, 1])
    maxY = torch.max(arena_corners_tensor[:, 1])
    minZ = torch.min(arena_corners_tensor[:, 2])
    maxZ = torch.max(arena_corners_tensor[:, 2])

    centerX = (minX + maxX) / 2
    centerY = (minY + maxY) / 2
    centerZ = (minZ + maxZ) / 2
    arena_center = torch.tensor([centerX, centerY, centerZ], dtype=coord_mat.dtype, device=coord_mat.device)
    sequence_length, num_markers, _ = coord_mat.shape
    rat1_images = []
    rat2_images = []
    stacked_images = []
    rand_int = random.randint(1, 1000)

    for frame in range(sequence_length):
        distances = []
        azimuthal_angles = []
        polar_angles = []
        for marker in range(num_markers):
            x, y, z = coord_mat[frame, marker, :].numpy()
            centered_x = x - arena_center[0]
            centered_y = y - arena_center[1]
            centered_z = z - arena_center[2]
            zero_vec = np.array([0, 0, 0])
            distance, azimuthal_angle, polar_angle = calculate_features([centered_x, centered_y, centered_z], zero_vec)
            distances.append(distance)
            azimuthal_angles.append(azimuthal_angle)
            polar_angles.append(polar_angle)
        max_distance = max(distances) if distances else 1
        distances_norm = [d / max_distance for d in distances]
        azimuthal_angles_norm = [(np.cos(np.deg2rad(a)) + 1) / 2 for a in azimuthal_angles]
        polar_angles_norm = [a / 180 for a in polar_angles]
        half_markers = num_markers // 2
        rgb_image_row_rat1 = np.stack([distances_norm[0:half_markers],
                                       azimuthal_angles_norm[0:half_markers], polar_angles_norm[0:half_markers]], axis=-1)
        rgb_image_row_rat2 = np.stack([distances_norm[half_markers:],
                                       azimuthal_angles_norm[half_markers:], polar_angles_norm[half_markers:]], axis=-1)
        rat1_images.append(rgb_image_row_rat1)
        rat2_images.append(rgb_image_row_rat2)
    stacked_images.append(np.stack(rat1_images, axis=0))
    stacked_images.append(np.stack(rat2_images, axis=0))

    concatenated_images = np.concatenate(stacked_images, axis=1)

    return concatenated_images


def calculate_features(coords1, coords2):
    vector = coords2 - coords1
    distance = np.linalg.norm(vector)
    azimuthal_angle = np.rad2deg(np.arctan2(vector[1], vector[0]))
    polar_angle = np.rad2deg(np.arccos(vector[2] / distance)) if distance != 0 else 0
    return distance, azimuthal_angle, polar_angle


def build_coord_mat(coord_mat_rat1, coord_mat_rat2):
    return torch.cat((coord_mat_rat1, coord_mat_rat2), dim=1)  # Shape: [30, 18, 3]


def rotation_matrix(rot_axis, theta):
    rot_axis = rot_axis / np.linalg.norm(rot_axis)
    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)
    minus_cos = 1 - cos_theta
    x, y, z = rot_axis

    rot_matrix = np.array([
        [cos_theta + x ** 2 * minus_cos, x * y * minus_cos - z * sin_theta,
         x * z * minus_cos + y * sin_theta],
        [y * x * minus_cos + z * sin_theta, cos_theta + y ** 2 * minus_cos,
         y * z * minus_cos - x * sin_theta],
        [z * x * minus_cos - y * sin_theta, z * y * minus_cos + x * sin_theta,
         cos_theta + z ** 2 * minus_cos]
    ])

    return rot_matrix


def align_plane_and_rotate(coord_mat, rot_mat, aligned_corners):

    for i in range(coord_mat.shape[0]):
        coord_mat_np = coord_mat[i, :, :].numpy()
        rotated_coords_np = np.dot(rot_mat, coord_mat_np.T).T
        coord_mat[i, :, :] = torch.from_numpy(rotated_coords_np).type_as(coord_mat)

    return coord_mat, aligned_corners


def augment_coord_mat(coord_mat, arena_corners):
    arena_corners_tensor = torch.tensor(arena_corners, dtype=coord_mat.dtype, device=coord_mat.device)

    minX = torch.min(arena_corners_tensor[:, 0])
    maxX = torch.max(arena_corners_tensor[:, 0])
    minY = torch.min(arena_corners_tensor[:, 1])
    maxY = torch.max(arena_corners_tensor[:, 1])

    centerX = (minX + maxX) / 2
    centerY = (minY + maxY) / 2

    angle = 2 * np.pi * np.random.rand()

    cos_angle = np.cos(angle)
    sin_angle = np.sin(angle)
    rot_z = torch.tensor([
        [cos_angle, -sin_angle, 0],
        [sin_angle, cos_angle, 0],
        [0, 0, 1]
    ], dtype=coord_mat.dtype, device=coord_mat.device)

    mirror_x = np.random.rand() > 0.5
    mirror_y = np.random.rand() > 0.5
    trans_factor_x = np.random.rand()
    trans_factor_y = np.random.rand()

    for i in range(coord_mat.shape[0]):
        coord_mat[i, :, :2] -= torch.tensor([centerX.item(), centerY.item()], dtype=coord_mat.dtype,
                                            device=coord_mat.device)

        coord_mat[i, :, :2] = torch.mm(coord_mat[i, :, :2], rot_z[:2, :2].t())

        if mirror_x:
            coord_mat[i, :, 0] = 2 * centerX - coord_mat[i, :, 0]
        if mirror_y:
            coord_mat[i, :, 1] = 2 * centerY - coord_mat[i, :, 1]

        coord_mat[i, :, :2] += torch.tensor([centerX.item() * trans_factor_x, centerY.item() * trans_factor_y],
                                            dtype=coord_mat.dtype,
                                            device=coord_mat.device)

    return coord_mat


def swap_markers(coord_mat, num_markers):

    first_half = coord_mat[:, :num_markers, :]
    second_half = coord_mat[:, num_markers:, :]

    swapped_mat = torch.cat((second_half, first_half), dim=1)

    return swapped_mat


def _preprocess_image(img_path, apply_augmentation=False, rotation_degree=0, brightness_factor=1.0,
                      contrast_factor=1.0, flip=0):
    img = Image.open(img_path).convert("RGB")

    img_transforms = Compose([
        Resize((112, 112), interpolation=InterpolationMode.BILINEAR),
        ToTensor(),
        Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])
    img_tensor = img_transforms(img)

    if apply_augmentation:
        # if flip == 1:
        #     img_tensor = F.vflip(img_tensor)
        img_tensor = F.rotate(img_tensor, rotation_degree)
        img_tensor = F.adjust_brightness(img_tensor, brightness_factor)
        img_tensor = F.adjust_contrast(img_tensor, contrast_factor)

    return img_tensor


def normalize_image(img):
    img_min, img_max = img.min(), img.max()
    return (img - img_min) / (img_max - img_min)
