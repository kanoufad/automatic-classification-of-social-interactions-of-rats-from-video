function  [passive] = is_passive(rat1_points, rat2_points, min_distance_passive, num_frames,std_threshold,cooldown_frames_passive, passive_speed_threshold,speed1,speed2)
    cooldown_frames = cooldown_frames_passive;
    num_markers = 9;
    passive = zeros(num_frames,1);
    saved_std = [];

    speed1 = [0;speed1];
    speed2 = [0;speed2];
    for i = 1:num_frames-cooldown_frames
        if mean(speed1(i:i+cooldown_frames),'omitnan') < passive_speed_threshold && mean(speed2(i:i+cooldown_frames),'omitnan') < passive_speed_threshold
            curr_dist = norm(mean(rat1_points{i},1) - mean(rat2_points{i},1));
            future_dist = norm(mean(rat1_points{i+cooldown_frames},1) - mean(rat2_points{i+cooldown_frames},1));
            if future_dist < min_distance_passive && curr_dist < min_distance_passive
                points_r1_seq_std = 0;
                points_r2_seq_std = 0;
                for j = 1:num_markers
                    cat_points_cl_r1 = cat(2, rat1_points{i:i+cooldown_frames});
                    cat_points_cl_r2 = cat(2, rat2_points{i:i+cooldown_frames});
    
                    point_r1_all_frames = cat_points_cl_r1(j,:);
                    point_r2_all_frames = cat_points_cl_r2(j,:);
    
                    points_r1_seq_std = points_r1_seq_std + std(point_r1_all_frames, 0, 2);
                    points_r2_seq_std = points_r2_seq_std + std(point_r2_all_frames, 0, 2);
                end
                points_r1_seq_std = points_r1_seq_std/num_markers;
                points_r2_seq_std = points_r2_seq_std/num_markers;
    
                saved_std = [saved_std, points_r1_seq_std];
                if points_r1_seq_std + points_r2_seq_std < std_threshold
                    passive(i:i+cooldown_frames) = 1;
                end
            end
        end
    end
    % figure;
    % histogram(saved_std)
end