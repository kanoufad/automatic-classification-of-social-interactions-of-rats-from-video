function ear_coord = compute_ear_3D(points, ear_idx, scale_factor)
    other_ear_idx = 3 - ear_idx + 2;
    spine_idx = 4;
    
    mid_point = (points(spine_idx,:) + points(1,:)) / 2;
    mid_point = (points(spine_idx,:) + mid_point) / 2;
    v = mid_point - points(other_ear_idx,:);
    v = v / norm(v);
    
    distance = norm(points(1,:) - mid_point);
    
    ear_coord = points(other_ear_idx,:) + scale_factor * distance * v;
end