function save_features(features, n_frames, model_data_folder, start_time, end_time)

    
    features_folder = fullfile(model_data_folder, 'features_deep');
    if ~exist(features_folder, 'dir')
        mkdir(features_folder);
    end
    features_folder_mat = fullfile(model_data_folder, 'features_deep_mat');

    if ~exist(features_folder_mat, 'dir')
        mkdir(features_folder_mat);
    end
    csv_file_path = fullfile(features_folder, sprintf("%d_%d_features.csv", start_time, end_time));
    save(fullfile(features_folder_mat, sprintf("%d_%d_features.mat", start_time, end_time)),"features");
    features_tab = struct2table(features);
    writetable(features_tab, csv_file_path);
    
    disp('features saved');

    

end
