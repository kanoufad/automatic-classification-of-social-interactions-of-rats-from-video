clear all;

% data_folder = 'E:\data\ad3_ad4_10m'

model_images_data_path = fullfile(data_folder,'model_data','images');
Folder = model_images_data_path;
if ~exist(Folder, 'dir')
        mkdir(Folder);
end
vid=VideoReader(fullfile(data_folder,'cam1.mp4'));

numFrames = vid.NumFrames;
n=numFrames;

for iFrame = 1:n
    if (mod(iFrame,1) == 0)
        frames = read(vid, iFrame);
        imwrite(frames, fullfile(Folder, sprintf('%d_1.jpg', iFrame)));
    end
end 
disp('cam1 done')

vid=VideoReader(fullfile(data_folder,'cam2.mp4'));

numFrames = vid.NumFrames;
n=numFrames;

for iFrame = 1:n
    if (mod(iFrame,1) == 0)
        frames = read(vid, iFrame);
        imwrite(frames, fullfile(Folder, sprintf('%d_2.jpg', iFrame)));
    end
end 
disp('cam2 done')

vid=VideoReader(fullfile(data_folder,'cam3.mp4'));

numFrames = vid.NumFrames;
n=numFrames

for iFrame = 1:n
    if (mod(iFrame,1) == 0)
        frames = read(vid, iFrame);
        imwrite(frames, fullfile(Folder, sprintf('%d_3.jpg', iFrame)));
    end
end 
disp('cam3 done')


vid=VideoReader(fullfile(data_folder,'cam4.mp4'));
numFrames = vid.NumFrames;
n=numFrames;

for iFrame = 1:n
    if (mod(iFrame,1) == 0)
        frames = read(vid, iFrame);
        imwrite(frames, fullfile(Folder, sprintf('%d_4.jpg', iFrame)));
    end
end 
disp('cam4 done')

main_folder = data_folder;
video_files = dir(fullfile(main_folder, '*.mp4'));
pythonEnv = 'C:\Users\fgu032lab034\Anaconda3\envs\deeplabcut\python.exe';

model_data_folder = fullfile(main_folder, 'model_data');
if ~exist(model_data_folder, 'dir')
    mkdir(model_data_folder);
end
gap_time = 20;
total_time_minutes = 9;
total_time_seconds = total_time_minutes*60;
global start_time current_time
start_time = 5;
addional_time_cut = 5;
cnt = 1;
for current_time = start_time:gap_time:total_time_seconds   
    end_time = current_time + gap_time;  % End time in seconds
    if end_time >= total_time_seconds
        break;
    end
    for i = 1:size(video_files,1)
        video_batch = fullfile(main_folder, video_files(i).name);
        
        [~, video_name, ~] = fileparts(video_batch);
        video_folder = fullfile(model_data_folder, video_name);
        if ~exist(video_folder, 'dir')
            mkdir(video_folder);
        end
        
        cut_folder_name = sprintf('%d_%d', current_time, end_time);
        cut_folder = fullfile(video_folder, cut_folder_name);
        if ~exist(cut_folder, 'dir')
            mkdir(cut_folder);
        end
        
        ffmpeg_path = 'D:\fadipokus\matlabfadi\main_project\ffmpeg-master-latest-win64-gpl\bin\ffmpeg.exe';

        cut_video_path = fullfile(cut_folder, sprintf('%s.mp4', cut_folder_name));
        command = sprintf('%s -ss %d -i %s -t %d -c:v copy -an %s', ffmpeg_path,current_time-addional_time_cut, video_batch,2*addional_time_cut + end_time - current_time, cut_video_path);

        [status, result] = system(command);

        if status == 0
            disp('Video cut successfully');
        else
            disp(['Error occurred: ', result]);
        end

        pythonScript = 'C:\Users\fgu032lab034\PycharmProjects\dlc_analyze_own\main.py';

        command = sprintf('%s %s %s %s', pythonEnv, pythonScript, cut_video_path,cut_folder);
        [status, result] = system(command);
        output_file = fullfile(cut_folder, 'output_file.txt');
        while ~exist(output_file, 'file')
            pause(1); 
        end
        if status == 0
            disp('DeepLabCut analysis completed successfully');
            load_str = "\" + cut_folder_name+ "DLC_dlcrnetms5_dlc_newAug18shuffle6_60000_sk.csv";
            data_dlc{i} = readtable(append(cut_folder,load_str));
        else
            disp(['Error occurred: ', result]);
        end
    end
    data_full_time{cnt} = data_dlc;
    cnt = cnt + 1;
end


stereo12 = load('calibration\cam_calib_09_08_6month_later\stereo12.mat');
stereo13 = load('calibration\cam_calib_09_08_6month_later\stereo13.mat');
stereo14 = load('calibration\cam_calib_09_08_6month_later\stereo14.mat');


initialTranslation = [0 0 0];
rotation = [0 0 0];

pose1 = rigidtform3d(rotation,initialTranslation);
pose2 = stereo12.stereoParams12.PoseCamera2;
pose3 = stereo13.stereoParams13.PoseCamera2;
pose4 = stereo14.stereoParams14.PoseCamera2;
global camMatrix1 camMatrix2 camMatrix3 camMatrix4
% Camera 1
camMatrix1 = [stereo12.stereoParams12.CameraParameters1.IntrinsicMatrix' [0; 0; 0]];

% Camera 2
camMatrix2 = stereo12.stereoParams12.CameraParameters2.IntrinsicMatrix' * [pose2.Rotation' pose2.Translation'];

% Camera 3
camMatrix3 = stereo13.stereoParams13.CameraParameters2.IntrinsicMatrix' * [pose3.Rotation' pose3.Translation'];

% Camera 4
camMatrix4 = stereo14.stereoParams14.CameraParameters2.IntrinsicMatrix' * [pose4.Rotation' pose4.Translation'];

rat_data_folder = main_folder;

im_folder = fullfile(main_folder, 'model_data', 'images');


cnt = 1;
for current_time = start_time:gap_time:total_time_seconds
    end_time = current_time + gap_time;  % End time in seconds
    if end_time >= total_time_seconds
        break;
    end
    disp(current_time)
    data_dlc = data_full_time{cnt};
    cam1labels = data_dlc{1,1};
    cam2labels = data_dlc{1,2};
    cam3labels = data_dlc{1,3};
    cam4labels = data_dlc{1,4};
    fps = 30;
    start_index = addional_time_cut*30+1;
    end_index = fps*gap_time + start_index - 1;

    if (size(cam1labels,1) < end_index || size(cam2labels,1) < end_index || size(cam3labels,1) < end_index || size(cam4labels,1) < end_index)
        cam1all{cnt} = [];
        cam2all{cnt} = [];
        cam3all{cnt} = [];
        cam4all{cnt} = [];
        cnt = cnt + 1;
        continue;
    end

    % CAM 1
    
    cam1points.snout_r1 = table2array(cam1labels(start_index:end_index,(2:4)));
    cam1points.left_ear_r1 = table2array(cam1labels(start_index:end_index,(5:7)));
    cam1points.right_ear_r1 = table2array(cam1labels(start_index:end_index,(8:10)));
    cam1points.spine1_r1 = table2array(cam1labels(start_index:end_index,11:13));
    cam1points.spine2_r1 = table2array(cam1labels(start_index:end_index,14:16));
    cam1points.spine3_r1 = table2array(cam1labels(start_index:end_index,17:19));
    cam1points.spine4_r1 = table2array(cam1labels(start_index:end_index,20:22));
    cam1points.spine5_r1 = table2array(cam1labels(start_index:end_index,23:25));
    cam1points.tail_base_r1 = table2array(cam1labels(start_index:end_index,26:28));
    
    cam1points.snout_r2 = table2array(cam1labels(start_index:end_index,(29:31)));
    cam1points.left_ear_r2 = table2array(cam1labels(start_index:end_index,(32:34)));
    cam1points.right_ear_r2 = table2array(cam1labels(start_index:end_index,(35:37)));
    cam1points.spine1_r2 = table2array(cam1labels(start_index:end_index,38:40));
    cam1points.spine2_r2 = table2array(cam1labels(start_index:end_index,41:43));
    cam1points.spine3_r2 = table2array(cam1labels(start_index:end_index,44:46));
    cam1points.spine4_r2 = table2array(cam1labels(start_index:end_index,47:49));
    cam1points.spine5_r2 = table2array(cam1labels(start_index:end_index,50:52));
    cam1points.tail_base_r2 = table2array(cam1labels(start_index:end_index,53:55));
    
    % CAM 2
    
    cam2points.snout_r1 = table2array(cam2labels(start_index:end_index,(2:4)));
    cam2points.left_ear_r1 = table2array(cam2labels(start_index:end_index,(5:7)));
    cam2points.right_ear_r1 = table2array(cam2labels(start_index:end_index,(8:10)));
    cam2points.spine1_r1 = table2array(cam2labels(start_index:end_index,11:13));
    cam2points.spine2_r1 = table2array(cam2labels(start_index:end_index,14:16));
    cam2points.spine3_r1 = table2array(cam2labels(start_index:end_index,17:19));
    cam2points.spine4_r1 = table2array(cam2labels(start_index:end_index,20:22));
    cam2points.spine5_r1 = table2array(cam2labels(start_index:end_index,23:25));
    cam2points.tail_base_r1 = table2array(cam2labels(start_index:end_index,26:28));
    
    cam2points.snout_r2 = table2array(cam2labels(start_index:end_index,(29:31)));
    cam2points.left_ear_r2 = table2array(cam2labels(start_index:end_index,(32:34)));
    cam2points.right_ear_r2 = table2array(cam2labels(start_index:end_index,(35:37)));
    cam2points.spine1_r2 = table2array(cam2labels(start_index:end_index,38:40));
    cam2points.spine2_r2 = table2array(cam2labels(start_index:end_index,41:43));
    cam2points.spine3_r2 = table2array(cam2labels(start_index:end_index,44:46));
    cam2points.spine4_r2 = table2array(cam2labels(start_index:end_index,47:49));
    cam2points.spine5_r2 = table2array(cam2labels(start_index:end_index,50:52));
    cam2points.tail_base_r2 = table2array(cam2labels(start_index:end_index,53:55));
    
    % CAM 3
    
    cam3points.snout_r1 = table2array(cam3labels(start_index:end_index,(2:4)));
    cam3points.left_ear_r1 = table2array(cam3labels(start_index:end_index,(5:7)));
    cam3points.right_ear_r1 = table2array(cam3labels(start_index:end_index,(8:10)));
    cam3points.spine1_r1 = table2array(cam3labels(start_index:end_index,11:13));
    cam3points.spine2_r1 = table2array(cam3labels(start_index:end_index,14:16));
    cam3points.spine3_r1 = table2array(cam3labels(start_index:end_index,17:19));
    cam3points.spine4_r1 = table2array(cam3labels(start_index:end_index,20:22));
    cam3points.spine5_r1 = table2array(cam3labels(start_index:end_index,23:25));
    cam3points.tail_base_r1 = table2array(cam3labels(start_index:end_index,26:28));
    
    cam3points.snout_r2 = table2array(cam3labels(start_index:end_index,(29:31)));
    cam3points.left_ear_r2 = table2array(cam3labels(start_index:end_index,(32:34)));
    cam3points.right_ear_r2 = table2array(cam3labels(start_index:end_index,(35:37)));
    cam3points.spine1_r2 = table2array(cam3labels(start_index:end_index,38:40));
    cam3points.spine2_r2 = table2array(cam3labels(start_index:end_index,41:43));
    cam3points.spine3_r2 = table2array(cam3labels(start_index:end_index,44:46));
    cam3points.spine4_r2 = table2array(cam3labels(start_index:end_index,47:49));
    cam3points.spine5_r2 = table2array(cam3labels(start_index:end_index,50:52));
    cam3points.tail_base_r2 = table2array(cam3labels(start_index:end_index,53:55));
    
    % % CAM 4
    
    cam4points.snout_r1 = table2array(cam4labels(start_index:end_index,(2:4)));
    cam4points.left_ear_r1 = table2array(cam4labels(start_index:end_index,(5:7)));
    cam4points.right_ear_r1 = table2array(cam4labels(start_index:end_index,(8:10)));
    cam4points.spine1_r1 = table2array(cam4labels(start_index:end_index,11:13));
    cam4points.spine2_r1 = table2array(cam4labels(start_index:end_index,14:16));
    cam4points.spine3_r1 = table2array(cam4labels(start_index:end_index,17:19));
    cam4points.spine4_r1 = table2array(cam4labels(start_index:end_index,20:22));
    cam4points.spine5_r1 = table2array(cam4labels(start_index:end_index,23:25));
    cam4points.tail_base_r1 = table2array(cam4labels(start_index:end_index,26:28));
    
    cam4points.snout_r2 = table2array(cam4labels(start_index:end_index,(29:31)));
    cam4points.left_ear_r2 = table2array(cam4labels(start_index:end_index,(32:34)));
    cam4points.right_ear_r2 = table2array(cam4labels(start_index:end_index,(35:37)));
    cam4points.spine1_r2 = table2array(cam4labels(start_index:end_index,38:40));
    cam4points.spine2_r2 = table2array(cam4labels(start_index:end_index,41:43));
    cam4points.spine3_r2 = table2array(cam4labels(start_index:end_index,44:46));
    cam4points.spine4_r2 = table2array(cam4labels(start_index:end_index,47:49));
    cam4points.spine5_r2 = table2array(cam4labels(start_index:end_index,50:52));
    cam4points.tail_base_r2 = table2array(cam4labels(start_index:end_index,53:55));

    % POINTS 3D
    
    n_frames = size(cam1points.snout_r1,1);
    
    % SETTINGS:
    fps = 30;
    freq = 1/fps;
    fn = fieldnames(cam1points);
    plot_data = 0;
    start = 1;
    % method = "stereo_triangulation";
    method = "direct_lin_svd"
    
    global currentFrame final_points3D_r1 final_points3D_r2;
    currentFrame = 1;
    
    if plot_data == 1
        hFigure = figure('Position', [100, 100, 800, 600]);
        
        nextFrameButton = uicontrol('Style', 'pushbutton', 'String', 'Next Frame',...
                'Position', [20 20 80 40],...
                'Callback', @nextFrameButtonCallback);
    end
    
    % CHECK RAT ID
    
    figure('Name', 'Manual Rat Labeling');
        
    fields_rat1 = fn(1:numel(fn)/2);
    fields_rat2 = fn(numel(fn)/2 + 1:end);
    
    for frame_idx = 1:n_frames
        all_points_valid = true; 
        
        for k = 1:numel(fn)
            point_c1 = cam1points.(fn{k})(frame_idx, 1:2);
            point_c2 = cam2points.(fn{k})(frame_idx, 1:2);
            point_c3 = cam3points.(fn{k})(frame_idx, 1:2);
            point_c4 = cam4points.(fn{k})(frame_idx, 1:2);
            
            if any(isnan(point_c1)) || any(isnan(point_c2)) || any(isnan(point_c3)) || any(isnan(point_c4))
                all_points_valid = false;
                break;  
            end
        end
        
        if all_points_valid
            first_valid_frame = frame_idx;
            break;
        end
    end
    if ~isnan(first_valid_frame)
        disp(['The first frame where all points in all views are not NaN is: ', num2str(first_valid_frame)]);
    else
        disp('No frame found where all points in all views are not NaN.');
    end
    
    shift_frame = first_valid_frame;
    maxDistanceFrameIndex = (current_time*30)-1+shift_frame;
    for k=1:numel(fn)
            
            points_c1(k,:) = cam1points.(fn{k})(shift_frame,1:2);
            points_c2(k,:) = cam2points.(fn{k})(shift_frame,1:2);
            points_c3(k,:) = cam3points.(fn{k})(shift_frame,1:2);
            points_c4(k,:) = cam4points.(fn{k})(shift_frame,1:2);
            
    end
    
    for camIndex = 1:4
        im_cam = imread(fullfile(im_folder,num2str(maxDistanceFrameIndex) + "_" + num2str(camIndex) + ".jpg"));
        
        subplot(2,2,camIndex);
        imshow(im_cam);
        title('Camera ' + string(camIndex));
        hold on;
    
        points = eval(['points_c' num2str(camIndex)]);
    
        scatter(points(1:9,1), points(1:9,2), 'g');
        hold on;
        scatter(points(10:18,1), points(10:18,2), 'r');
    
        title('Click on a point from Rat 1');
        [x,y] = ginput(1);
        
        mean_rat1 = mean(points(1:9, :), 1,'omitmissing');
        mean_rat2 = mean(points(10:18, :), 1,'omitmissing');
        
        distance_to_rat1 = norm([x,y] - mean_rat1);
        distance_to_rat2 = norm([x,y] - mean_rat2);
        
        if distance_to_rat2 < distance_to_rat1
            disp('swap')
            varName = sprintf('cam%dpoints', camIndex);
            new_camPoints = eval(varName);
    
            for i = 1:numel(fields_rat1)
                for frameIndex = 1:n_frames
                    bodyPart1 = fields_rat1{i};
                    bodyPart2 = fields_rat2{i};
                    temp = new_camPoints.(bodyPart1)(frameIndex, :);
                    new_camPoints.(bodyPart1)(frameIndex, :) = new_camPoints.(bodyPart2)(frameIndex, :);
                    new_camPoints.(bodyPart2)(frameIndex, :) = temp;
                end
            end
    
            eval([varName '= new_camPoints']);
    
        end
    
        hold off;
    
    end
    cam1all{cnt} = cam1points;
    cam2all{cnt} = cam2points;
    cam3all{cnt} = cam3points;
    cam4all{cnt} = cam4points;
    cnt = cnt + 1;
    close all;
end

cntall = 1;
for current_time = start_time:gap_time:total_time_seconds
    end_time = current_time + gap_time; 
    cam1points = cam1all{cntall};
    cam2points = cam2all{cntall};
    cam3points = cam3all{cntall};
    cam4points = cam4all{cntall};
    if (isempty(cam1points) || isempty(cam2points) || isempty(cam3points) || isempty(cam4points))
        cntall = cntall + 1;
        continue
    end
    cntall = cntall + 1;
    for i = 1:n_frames
        for k=1:numel(fn)
            points_c1(k,:) = cam1points.(fn{k})(i,1:2);
            points_c2(k,:) = cam2points.(fn{k})(i,1:2);
            points_c3(k,:) = cam3points.(fn{k})(i,1:2);
            points_c4(k,:) = cam4points.(fn{k})(i,1:2);
        end
    
        points_all_rat1_c1{i}(:,1) = points_c1(1:9,1);
        points_all_rat1_c1{i}(:,2) = points_c1(1:9,2);
        points_all_rat2_c1{i}(:,1) = points_c1(10:18,1);
        points_all_rat2_c1{i}(:,2) = points_c1(10:18,2);
        
        points_all_rat1_c2{i}(:,1) = points_c2(1:9,1);
        points_all_rat1_c2{i}(:,2) = points_c2(1:9,2);
        points_all_rat2_c2{i}(:,1) = points_c2(10:18,1);
        points_all_rat2_c2{i}(:,2) = points_c2(10:18,2);
        
        points_all_rat1_c3{i}(:,1) = points_c3(1:9,1);
        points_all_rat1_c3{i}(:,2) = points_c3(1:9,2);
        points_all_rat2_c3{i}(:,1) = points_c3(10:18,1);
        points_all_rat2_c3{i}(:,2) = points_c3(10:18,2);
        
        points_all_rat1_c4{i}(:,1) = points_c4(1:9,1);
        points_all_rat1_c4{i}(:,2) = points_c4(1:9,2);
        points_all_rat2_c4{i}(:,1) = points_c4(10:18,1);
        points_all_rat2_c4{i}(:,2) = points_c4(10:18,2);
    end
    point3D_time = [];
    plot_data = 0;
    method = "direct_lin_svd"
    if plot_data == 1
        hFigure = figure('Position', [100, 100, 800, 600]);
        
        nextFrameButton = uicontrol('Style', 'pushbutton', 'String', 'Next Frame',...
                'Position', [20 20 80 40],...
                'Callback', @nextFrameButtonCallback);
    end
    
    for i = 1:n_frames
        disp(i)
        if ~isempty(points_all_rat1_c1{i})
            if method == "direct_lin_svd"
                points3D_combined = direct_lin_svd_w_nan(points_all_rat1_c1{i}, points_all_rat1_c2{i}, points_all_rat1_c3{i}, points_all_rat1_c4{i});
                final_points3D_r1 = points3D_combined;
                points3D_combined = direct_lin_svd_w_nan(points_all_rat2_c1{i}, points_all_rat2_c2{i}, points_all_rat2_c3{i}, points_all_rat2_c4{i});
                final_points3D_r2 = points3D_combined;
            end
            points3D_time_r1(:,:,i) = final_points3D_r1;
            points3D_time_r2(:,:,i) = final_points3D_r2;

        end
    end
    for i = 1: size(points3D_time_r1,3)
        points_3d_r1{i} = points3D_time_r1(:,:,i);
        points_3d_r2{i} = points3D_time_r2(:,:,i);
        points_3d_both{i}(1:9,:) = points3D_time_r1(:,:,i);
        points_3d_both{i}(10:18,:) = points3D_time_r2(:,:,i);
    end
    
    % PDM:
    % VALID COORDINATES FROM 4 VIEWS:
    likelihood_threshold = 0.95;
    num_markers = size(points_3d_r1{1},1);
    for i = 1:n_frames
               
        frame_counter_show = sprintf('frame: %d / %d',i,n_frames);
        disp(frame_counter_show)
    
        for k=1:numel(fn)
            
            likelihood(k,i) = cam1points.(fn{k})(i,3)+cam2points.(fn{k})(i,3)+cam3points.(fn{k})(i,3)+cam4points.(fn{k})(i,3);
            likelihood_c1(k) = cam1points.(fn{k})(i,3);
            likelihood_c2(k) = cam2points.(fn{k})(i,3);
            likelihood_c3(k) = cam3points.(fn{k})(i,3);
            likelihood_c4(k) = cam4points.(fn{k})(i,3);
    
        end
        valid_learning_coordinates_3D_r1{i} = process_by_likelihood(points_3d_r1{i}, likelihood_c1(1:9),likelihood_c2(1:9),likelihood_c3(1:9),likelihood_c4(1:9),likelihood_threshold,num_markers);
        valid_learning_coordinates_3D_r2{i} = process_by_likelihood(points_3d_r2{i}, likelihood_c1(10:18),likelihood_c2(10:18),likelihood_c3(10:18),likelihood_c4(10:18),likelihood_threshold,num_markers);
    
    end
    valid_learning_idx = ~cellfun(@(x) any(isnan(x(:))), valid_learning_coordinates_3D_r1);
    valid_learning_coordinates_3D_r1 = valid_learning_coordinates_3D_r1(valid_learning_idx);
    
    valid_learning_idx = ~cellfun(@(x) any(isnan(x(:))), valid_learning_coordinates_3D_r2);
    valid_learning_coordinates_3D_r2 = valid_learning_coordinates_3D_r2(valid_learning_idx);
    
    valid_learning_coordinates = [valid_learning_coordinates_3D_r1, valid_learning_coordinates_3D_r2];
    
    % PDM:
    end_pdm_training = size(valid_learning_coordinates,2);
    
    ref_learn = valid_learning_coordinates{1};
    pref = [];
    for i = 1:size(ref_learn,1)
        pref = [pref, ref_learn(i,:)];
    end
    aligned_shapes{1} = ref_learn;
    
    for val_skelet = 2:end_pdm_training
    
        counter_show = sprintf('skeleton alligned: %d / %d',val_skelet,size(valid_learning_coordinates,2));
        disp(counter_show)
    
        move_learn = valid_learning_coordinates{val_skelet};
        movep = [];
        for i = 1:size(move_learn,1)
            movep = [movep, move_learn(i,:)];
        end
        weights_p = ones([num_markers, 1]);
        pref = pref(:);
        movep = movep(:);
        
        weights_p = ones([length(pref)/3, 1]);
        
        [ptransf, alpha, beta, gamma, s, tx, ty, tz] = pointalign3D(pref, movep, weights_p);
        aligned_shapes{val_skelet} = ptransf;
    end
    
    
    num_shapes = length(aligned_shapes);
    X = zeros(3 * num_markers, num_shapes);
    for i = 1:num_shapes
        X(:, i) = aligned_shapes{i}(:);
    end
    mean_shape = mean(X, 2);
    movep = [];
    mean_shape_alligned = [];
    for i = 1:size(mean_shape,1)
        movep = [movep, mean_shape(i,:)];
    end

    [mean_shape_alligned_t, alpha, beta, gamma, s, tx, ty, tz] = pointalign3D(pref, movep, weights_p);
    mean_shape_alligned_t = mean_shape_alligned_t';
    for i = 1:size(mean_shape_alligned_t,1)
        mean_shape_alligned = [mean_shape_alligned, mean_shape_alligned_t(i,:)];
    end
    
    X_centered = X - mean_shape_alligned';
    [coeff, score, latent] = pca(X_centered');
    
    
    explained = cumsum(latent) / sum(latent);
    
    numComponentsToKeep = find(explained >= 0.95, 1, 'first');
    
    [coeff, score, latent] = pca(X_centered', 'NumComponents', numComponentsToKeep);
    mean_shape = mean_shape_alligned';
    
    % GEOMETRICAL FILL R1:
    
    num_shapes = length(valid_learning_coordinates_3D_r1);
    scale_factors = linspace(0, 6, 200); 
    
    best_scale_factor_snout = scale_factors(1); 
    best_scale_factor_ear = scale_factors(1); 
    errors_snout = zeros(num_shapes, length(scale_factors)); 
    errors_ears = zeros(num_shapes, length(scale_factors)); 
    
    for i = 1:2
        remove_marker = i; 
        
        min_error = inf; 
    
        for s_idx = 1:length(scale_factors)
            scale_factor_snout = scale_factors(s_idx);
            scale_factor_ear = scale_factors(s_idx);
            for shape_idx = 1:num_shapes
                original_shape = valid_learning_coordinates_3D_r1{shape_idx};
                shape_to_fill = original_shape;
                shape_to_fill(remove_marker, :) = nan;
                
                unaligned_shape = compute_points_alg_3D(shape_to_fill, scale_factor_snout, scale_factor_ear);
                
                error = sum((original_shape(:) - unaligned_shape(:)).^2, 'omitnan');
                if i == 1
                    errors_snout(shape_idx, s_idx) = error;
                end
                if i == 2
                    errors_ears(shape_idx, s_idx) = error;
                end
                if error < min_error
                    min_error = error;
                    if i == 1
                        best_scale_factor_snout = scale_factor_snout;
                    end
                    if i == 2
                        best_scale_factor_ear = scale_factor_ear; 
                    end
    
                end
            end
        end
    end
    mean_errors_snout = mean(errors_snout, 1);
    mean_errors_ears = mean(errors_ears,1);
    
    % GEOMETRICAL FILL R2:
    
    num_shapes = length(valid_learning_coordinates_3D_r2);
    scale_factors = linspace(0, 6, 200); 
    
    best_scale_factor_snoutr2 = scale_factors(1); 
    best_scale_factor_earr2 = scale_factors(1); 
    errors_snoutr2 = zeros(num_shapes, length(scale_factors)); 
    errors_earsr2 = zeros(num_shapes, length(scale_factors)); 
    
    for i = 1:2
        remove_marker = i; 
        
        min_error = inf; 
    
        for s_idx = 1:length(scale_factors)
            scale_factor_snoutr2 = scale_factors(s_idx);
            scale_factor_earr2 = scale_factors(s_idx);
            for shape_idx = 1:num_shapes
                original_shape = valid_learning_coordinates_3D_r2{shape_idx};
                shape_to_fill = original_shape;
                shape_to_fill(remove_marker, :) = nan;
                
                unaligned_shape = compute_points_alg_3D(shape_to_fill, scale_factor_snoutr2, scale_factor_earr2);
                
                error = sum((original_shape(:) - unaligned_shape(:)).^2, 'omitnan');
                if i == 1
                    errors_snoutr2(shape_idx, s_idx) = error;
                end
                if i == 2
                    errors_earsr2(shape_idx, s_idx) = error;
                end
                if error < min_error
                    min_error = error;
                    if i == 1
                        best_scale_factor_snoutr2 = scale_factor_snoutr2;
                    end
                    if i == 2
                        best_scale_factor_earr2 = scale_factor_earr2; 
                    end
    
                end
            end
        end
    end
    mean_errors_snoutr2 = mean(errors_snoutr2, 1);
    mean_errors_earsr2 = mean(errors_earsr2,1);
    
    % LIKELIHOOD
    likelihood_r1_c1 = [];
    likelihood_r1_c2 = [];
    likelihood_r1_c3 = [];
    likelihood_r1_c4 = [];
    
    likelihood_r2_c1 = [];
    likelihood_r2_c2 = [];
    likelihood_r2_c3 = [];
    likelihood_r2_c4 = [];
    
    points_all_rat1_c1_orig = points_all_rat1_c1;
    points_all_rat2_c1_orig = points_all_rat2_c1;
    
    points_all_rat1_c2_orig = points_all_rat1_c2;
    points_all_rat2_c2_orig = points_all_rat2_c2;
    
    points_all_rat1_c3_orig = points_all_rat1_c3;
    points_all_rat2_c3_orig = points_all_rat2_c3;
    
    points_all_rat1_c4_orig = points_all_rat1_c4;
    points_all_rat2_c4_orig = points_all_rat2_c4;
    
    for i = 1:n_frames
               
        frame_counter_show = sprintf('frame: %d / %d',i,n_frames);
        disp(frame_counter_show)
    
        for k=1:numel(fn)
            
            likelihood(k,i) = cam1points.(fn{k})(i,3)+cam2points.(fn{k})(i,3)+cam3points.(fn{k})(i,3)+cam4points.(fn{k})(i,3);
            likelihood_c1(k) = cam1points.(fn{k})(i,3);
            likelihood_c2(k) = cam2points.(fn{k})(i,3);
            likelihood_c3(k) = cam3points.(fn{k})(i,3);
            likelihood_c4(k) = cam4points.(fn{k})(i,3);
    
        end
        
        likelihood_r1_c1 = [likelihood_r1_c1, sum(likelihood_c1(1:9))];
        likelihood_r1_c2 = [likelihood_r1_c2, sum(likelihood_c2(1:9))];
        likelihood_r1_c3 = [likelihood_r1_c3, sum(likelihood_c3(1:9))];
        likelihood_r1_c4 = [likelihood_r1_c4, sum(likelihood_c4(1:9))];
    
        likelihood_r2_c1 = [likelihood_r2_c1, sum(likelihood_c1(10:18))];
        likelihood_r2_c2 = [likelihood_r2_c2, sum(likelihood_c2(10:18))];
        likelihood_r2_c3 = [likelihood_r2_c3, sum(likelihood_c3(10:18))];
        likelihood_r2_c4 = [likelihood_r2_c4, sum(likelihood_c4(10:18))];
    
        if points_all_rat1_c1_orig{i} == points_all_rat2_c1_orig{i}
            points_all_rat1_c1_orig{i} = nan(9,2);
            points_all_rat2_c1_orig{i} = nan(9,2);
        end
    
        if points_all_rat1_c2_orig{i} == points_all_rat2_c2_orig{i}
            points_all_rat1_c2_orig{i} = nan(9,2);
            points_all_rat2_c2_orig{i} = nan(9,2);
        end
    
        if points_all_rat1_c3_orig{i} == points_all_rat2_c3_orig{i}
            points_all_rat1_c3_orig{i} = nan(9,2);
            points_all_rat2_c3_orig{i} = nan(9,2);
        end
    
        if points_all_rat1_c4_orig{i} == points_all_rat2_c4_orig{i}
            points_all_rat1_c4_orig{i} = nan(9,2);
            points_all_rat2_c4_orig{i} = nan(9,2);
        end
      
    
        if all(isnan(points_all_rat1_c1_orig{i}(:)))
            points_all_rat2_c1_orig{i} = nan(9, 2);
        end
        if all(isnan(points_all_rat1_c2_orig{i}(:)))
            points_all_rat2_c2_orig{i} = nan(9, 2);
        end
        if all(isnan(points_all_rat1_c3_orig{i}(:)))
            points_all_rat2_c3_orig{i} = nan(9, 2);
        end
        if all(isnan(points_all_rat1_c4_orig{i}(:)))
            points_all_rat2_c4_orig{i} = nan(9, 2);
        end
    
        if all(isnan(points_all_rat2_c1_orig{i}(:)))
            points_all_rat1_c1_orig{i} = nan(9, 2);
        end
        if all(isnan(points_all_rat2_c2_orig{i}(:)))
            points_all_rat1_c2_orig{i} = nan(9, 2);
        end
        if all(isnan(points_all_rat2_c3_orig{i}(:)))
            points_all_rat1_c3_orig{i} = nan(9, 2);
        end
        if all(isnan(points_all_rat2_c4_orig{i}(:)))
            points_all_rat1_c4_orig{i} = nan(9, 2);
        end
    
        for j = 1:9
            if likelihood_c1(j) < 0.6
                points_all_rat1_c1_orig{i}(j,1:2) = nan;
            end
            if likelihood_c2(j) < 0.6
                points_all_rat1_c2_orig{i}(j,1:2) = nan;
            end
            if likelihood_c3(j) < 0.6
                points_all_rat1_c3_orig{i}(j,1:2) = nan;
            end
            if likelihood_c4(j) < 0.6
                points_all_rat1_c4_orig{i}(j,1:2) = nan;
            end
        end
    
        for j = 1:9
            if likelihood_c1(j+9) < 0.6
                points_all_rat2_c1_orig{i}(j,1:2) = nan;
            end
            if likelihood_c2(j+9) < 0.6
                points_all_rat2_c2_orig{i}(j,1:2) = nan;
            end
            if likelihood_c3(j+9) < 0.6
                points_all_rat2_c3_orig{i}(j,1:2) = nan;
            end
            if likelihood_c4(j+9) < 0.6
                points_all_rat2_c4_orig{i}(j,1:2) = nan;
            end
        end
    end

    indices_view_rat1 = {};
    indices_view_rat2 = {};
    
    points_rat1_corrected_all = {};
    points_rat2_corrected_all = {};
    points_rat1_corrected_all{1} = {points_all_rat1_c1_orig{1},points_all_rat1_c2_orig{1},points_all_rat1_c3_orig{1},points_all_rat1_c4_orig{1}};
    points_rat2_corrected_all{1} = {points_all_rat2_c1_orig{1},points_all_rat2_c2_orig{1},points_all_rat2_c3_orig{1},points_all_rat2_c4_orig{1}};
    last_valid_idx = 1;
    for i = 2:n_frames
        frame_counter_show = sprintf('checking identity swap: %d / %d',i,n_frames);
        disp(frame_counter_show)
            
        [points_rat1_corrected_all{i}, points_rat2_corrected_all{i}, indices_view_rat1{i}, indices_view_rat2{i}, error_optimatization_rat1(:,i),last_valid_idx] = reprojection_error_3D_swap_optimatization(indices_view_rat1, indices_view_rat2, ...
            points_all_rat1_c1_orig{i}, points_all_rat1_c2_orig{i}, points_all_rat1_c3_orig{i}, points_all_rat1_c4_orig{i}, ...
            points_all_rat2_c1_orig{i}, points_all_rat2_c2_orig{i}, points_all_rat2_c3_orig{i}, points_all_rat2_c4_orig{i}, ...
            points_rat1_corrected_all, points_rat2_corrected_all,i,last_valid_idx);
     
    end
    
    for i = 1:n_frames
        points_all_rat1_c1_orig{i} = points_rat1_corrected_all{i}{1};
        points_all_rat1_c2_orig{i} = points_rat1_corrected_all{i}{2};
        points_all_rat1_c3_orig{i} = points_rat1_corrected_all{i}{3};
        points_all_rat1_c4_orig{i} = points_rat1_corrected_all{i}{4};

        points_all_rat2_c1_orig{i} = points_rat2_corrected_all{i}{1};
        points_all_rat2_c2_orig{i} = points_rat2_corrected_all{i}{2};
        points_all_rat2_c3_orig{i} = points_rat2_corrected_all{i}{3};
        points_all_rat2_c4_orig{i} = points_rat2_corrected_all{i}{4};
    end

    method = "direct_lin_svd";

    for i = 1:n_frames
        disp(i)
        if method == "direct_lin_svd"
            points3D_combined = direct_lin_svd_w_nan(points_all_rat1_c1_orig{i}, points_all_rat1_c2_orig{i}, points_all_rat1_c3_orig{i}, points_all_rat1_c4_orig{i});
            final_points3D_r1 = points3D_combined;
            points3D_combined = direct_lin_svd_w_nan(points_all_rat2_c1_orig{i}, points_all_rat2_c2_orig{i}, points_all_rat2_c3_orig{i}, points_all_rat2_c4_orig{i});
            final_points3D_r2 = points3D_combined;
        end
        points3D_time_r1_swap(:,:,i) = final_points3D_r1;
        points3D_time_r2_swap(:,:,i) = final_points3D_r2;
    end
    for i = 1: size(points3D_time_r1,3)
        points_3d_r1_swap{i} = points3D_time_r1_swap(:,:,i);
        points_3d_r2_swap{i} = points3D_time_r2_swap(:,:,i);
        points_3d_both_swap{i}(1:9,:) = points3D_time_r1_swap(:,:,i);
        points_3d_both_swap{i}(10:18,:) = points3D_time_r2_swap(:,:,i);
    end

    pc = coeff; % PCA

    for i = 1:size(points_3d_r1,2)
        disp(i)
        filled_points_geo_r1_swap{i} = compute_points_alg_3D(points_3d_r1_swap{i},best_scale_factor_snout, scale_factor_ear);
        filled_points_geo_r2_swap{i} = compute_points_alg_3D(points_3d_r2_swap{i},best_scale_factor_snoutr2, scale_factor_earr2);

        filled_points_PDM_r1_swap{i} = fill_missing_markers_PDM(filled_points_geo_r1_swap{i},mean_shape,pc,pref,weights_p);
        filled_points_PDM_r2_swap{i} = fill_missing_markers_PDM(filled_points_geo_r2_swap{i},mean_shape,pc,pref,weights_p);

    end



    points3D_optimized_rat1 = cell(1, n_frames);
    points3D_optimized_rat2 = cell(1, n_frames);
    error_optimatization_rat1 = [];
    error_optimatization_rat2 = [];
    for i = 1:n_frames
        disp(i)

        points3D_frame_rat1 = filled_points_PDM_r1_swap{i};

        [points3D_optimized_rat1{i}, error_optimatization_rat1(:,i)] = reprojection_error_3D_point_optimatization(points3D_frame_rat1, points_all_rat1_c1_orig{i}, points_all_rat1_c2_orig{i}, points_all_rat1_c3_orig{i}, points_all_rat1_c4_orig{i}, 1);

        points3D_frame_rat2 = filled_points_PDM_r2_swap{i};

        [points3D_optimized_rat2{i}, error_optimatization_rat2(:,i)] = reprojection_error_3D_point_optimatization(points3D_frame_rat2, points_all_rat2_c1_orig{i}, points_all_rat2_c2_orig{i}, points_all_rat2_c3_orig{i}, points_all_rat2_c4_orig{i}, 1);    

    end

    data_rat1 = [];
    data_rat2 = [];
    points3D_optimized_rat1_interp = {};
    points3D_optimized_rat2_interp = {};
    
    for i = 1:size(points3D_optimized_rat1,2)
        data_rat1(:,:,i) = points3D_optimized_rat1{i};
        data_rat2(:,:,i) = points3D_optimized_rat2{i};
    end
    
    max_gap = 15;
    filled_data_rat1 = fillmissing(data_rat1, 'pchip', 3,'MaxGap',max_gap);
    filled_data_rat2 = fillmissing(data_rat2, 'pchip', 3,'MaxGap',max_gap);
    
    for i = 1:size(points3D_optimized_rat1,2)
        points3D_optimized_rat1_interp{i} = filled_data_rat1(:,:,i);
        points3D_optimized_rat2_interp{i} = filled_data_rat2(:,:,i);
    end

    for i = 1: size(points3D_time_r1,3)
        scaled_points_3d_r1{i} = filled_data_rat1(:,:,i);
        scaled_points_3d_r2{i} = filled_data_rat2(:,:,i);
    end
    
    % MATH BEHAV MODEL
    %n_frames = size(scaled_3d_points_r1,3);
    delta_t = 1/30;
    
    for i = 2:n_frames
        % disp(i)
        if ~isempty(points_all_rat1_c1{i})
            features(i) = extract_features(scaled_points_3d_r1{i}, scaled_points_3d_r2{i}, scaled_points_3d_r1{i-1}, scaled_points_3d_r2{i-1}, delta_t);
        end
    end
    
    math_parameters = load('math_parameters.mat');
    params = math_parameters.best_params;
    acc_threshold = 0;

    predicted_labels = apply_model(scaled_points_3d_r1, scaled_points_3d_r2, n_frames, params.speed_threshold, ...
            acc_threshold, params.min_approach_distance, params.min_distance_after, params.cooldown_frames, features, ...
            params.thresholdh2h, params.thresholdh2b, params.std_threshold, params.min_distance_passive, ...
            params.cooldown_frames_passive, params.passive_speed_threshold);
    
    math_model_prediction_folder = fullfile(model_data_folder, 'math_model_detailed_predictions');
    if ~exist(math_model_prediction_folder, 'dir')
        mkdir(math_model_prediction_folder);
    end
    writematrix(predicted_labels',math_model_prediction_folder + "\" + sprintf("%d_%d_math_detailed_predictions.csv",current_time,end_time))
    disp('video done')

    save_points_3d_as_csv(filled_data_rat1, n_frames, model_data_folder, current_time, end_time,1);
    save_points_3d_as_csv(filled_data_rat2, n_frames, model_data_folder, current_time, end_time,2);
    save_features(features,n_frames,model_data_folder,current_time,end_time)

    num_frames = size(filled_data_rat1, 3); 
    sequence_length = 15; 
    
    seq_r1 = filled_data_rat1;
    seq_r2 = filled_data_rat2;
    
    rat1_file = sprintf('temp_r1.mat');
    rat2_file = sprintf('temp_r2.mat');
    save(rat1_file, 'seq_r1');
    save(rat2_file, 'seq_r2');
    
    dataset_path = main_folder;
    cmd = sprintf('C:\\Users\\fgu032lab034\\Anaconda3\\envs\\behav_model_new\\python.exe C:\\Users\\fgu032lab034\\PycharmProjects\\behav_model_new\\predictions.py %s %d %d %s %s', ...
                  dataset_path, current_time, end_time, rat1_file, rat2_file);
    [status, result] = system(cmd);
end



%% PLOTING SEQUENCES
hFigureFilt = figure('Position', [100, 100, 800, 600]);

nextFrameButton = uicontrol('Style', 'pushbutton', 'String', 'Next Frame',...
        'Position', [20 20 80 40],...
        'Callback', @nextFrameButtonCallback);
frame_idx = ((start_time*30)-1);

for i = 360:n_frames
    sgtitle(['Frame Index: ', num2str(i)]); 
    titleStr1 = 'Camera 1: ';
    titleStr2 = 'Camera 2: ';
    titleStr3 = 'Camera 3: ';
    titleStr4 = 'Camera 4: ';
    % end
    
    subplot(221)
    cla;
    im_cam = imread(fullfile(im_folder,num2str(frame_idx+i) + "_" + num2str(1) + ".jpg"));
    imshow(im_cam)
    hold on;
    points2Dr1 = reproject_view_frame(1, points3D_optimized_rat1_interp{i});
    points2Dr2 = reproject_view_frame(1, points3D_optimized_rat2_interp{i});

    scatter(points2Dr1(:,1),points2Dr1(:,2),'red')
    scatter(points2Dr2(:,1),points2Dr2(:,2),'blue')
    hold on;
    scatter(points_all_rat1_c1_orig{i}(:,1),points_all_rat1_c1_orig{i}(:,2),'yellow')
    scatter(points_all_rat2_c1_orig{i}(:,1),points_all_rat2_c1_orig{i}(:,2),'cyan')

    title(titleStr1);
    hold off;

    subplot(222)
    cla;
    im_cam = imread(fullfile(im_folder,num2str(frame_idx+i) + "_" + num2str(2) + ".jpg"));
    imshow(im_cam)
    hold on;
    points2Dr1 = reproject_view_frame(2, points3D_optimized_rat1_interp{i});
    points2Dr2 = reproject_view_frame(2, points3D_optimized_rat2_interp{i});

    scatter(points2Dr1(:,1),points2Dr1(:,2),'red')
    scatter(points2Dr2(:,1),points2Dr2(:,2),'blue')
    hold on;
    scatter(points_all_rat1_c2_orig{i}(:,1),points_all_rat1_c2_orig{i}(:,2),'yellow')
    scatter(points_all_rat2_c2_orig{i}(:,1),points_all_rat2_c2_orig{i}(:,2),'cyan')

    title(titleStr2);
    hold off;

    subplot(223)
    cla;
    im_cam = imread(fullfile(im_folder,num2str(frame_idx+i) + "_" + num2str(3) + ".jpg"));
    imshow(im_cam)
    hold on;
    points2Dr1 = reproject_view_frame(3, points3D_optimized_rat1_interp{i});
    points2Dr2 = reproject_view_frame(3, points3D_optimized_rat2_interp{i});

    scatter(points2Dr1(:,1),points2Dr1(:,2),'red')
    scatter(points2Dr2(:,1),points2Dr2(:,2),'blue')
    hold on;
    scatter(points_all_rat1_c3_orig{i}(:,1),points_all_rat1_c3_orig{i}(:,2),'yellow')
    scatter(points_all_rat2_c3_orig{i}(:,1),points_all_rat2_c3_orig{i}(:,2),'cyan')
    title(titleStr3);
    hold off;

    subplot(224)
    cla;
    im_cam = imread(fullfile(im_folder,num2str(frame_idx+i) + "_" + num2str(4) + ".jpg"));
    imshow(im_cam)
    hold on;
    points2Dr1 = reproject_view_frame(4, points3D_optimized_rat1_interp{i});
    points2Dr2 = reproject_view_frame(4, points3D_optimized_rat2_interp{i});

    scatter(points2Dr1(:,1),points2Dr1(:,2),'red')
    scatter(points2Dr2(:,1),points2Dr2(:,2),'blue')
    hold on;
    scatter(points_all_rat1_c4_orig{i}(:,1),points_all_rat1_c4_orig{i}(:,2),'yellow')
    scatter(points_all_rat2_c4_orig{i}(:,1),points_all_rat2_c4_orig{i}(:,2),'cyan')
    uiwait;
    % pause(0.05)
end