function points = compute_points_alg_3D(points, scale_factor_snout, scale_factor_ear)
    ear1_idx = 2;
    ear2_idx = 3;
    if isnan(points(1,:))
        if all(~isnan(points([ear1_idx, ear2_idx, 4], :)), 'all')
            points(1,:) = compute_nose_3D(points,scale_factor_snout);
        end
    end
    if isnan(points(ear1_idx,:))
        if all(~isnan(points([1, ear2_idx, 4], :)), 'all')
            points(ear1_idx,:) = compute_ear_3D(points,ear1_idx,scale_factor_ear);
        end
    end
    if isnan(points(ear2_idx,:))
        if all(~isnan(points([1, ear1_idx, 4], :)), 'all')
            points(ear2_idx,:) = compute_ear_3D(points,ear2_idx,scale_factor_ear);
        end
    end
end