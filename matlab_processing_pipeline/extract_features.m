%% FEATURES:
function features = extract_features(rat1, rat2, rat1_prev, rat2_prev, delta_t)
    head1 = mean(rat1(1:3,:),1); 
    head2 = mean(rat2(1:3,:),1);

    dist_Head2Head = norm(head1-head2);
    dist_Nose1Tailbase2 = norm(rat1(1,:)-rat2(9,:));
    dist_Nose2Tailbase1 = norm(rat2(1,:)-rat1(9,:));

    if isempty(rat1_prev) || isempty(rat2_prev)
        speed1 = 0;
        speed2 = 0;
    else
        speed1 = norm(mean(rat1(1:3,:),1) - mean(rat1_prev(1:3,:),1)) / delta_t;
        speed2 = norm(mean(rat2(1:3,:),1) - mean(rat2_prev(1:3,:),1)) / delta_t;
    end
    
    features = struct('dist_Head2Head', dist_Head2Head, 'dist_Nose2Tailbase1', dist_Nose2Tailbase1, 'dist_Nose1Tailbase2', dist_Nose1Tailbase2, 'speed1', speed1, 'speed2', speed2);
end
