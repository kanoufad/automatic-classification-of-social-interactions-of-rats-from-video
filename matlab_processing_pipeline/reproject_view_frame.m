
function points2D = reproject_view_frame(view, point3D_frame)
    global camMatrix1 camMatrix2 camMatrix3 camMatrix4
    points_frame = point3D_frame;
    switch view
        case 1
            reprojected_points = reproject_points(points_frame, camMatrix1);
        case 2
            reprojected_points = reproject_points(points_frame, camMatrix2);
        case 3
            reprojected_points = reproject_points(points_frame, camMatrix3);
        case 4
            reprojected_points = reproject_points(points_frame, camMatrix4);
    end
    points2D = reprojected_points;
end