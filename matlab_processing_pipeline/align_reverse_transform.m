function original_shape = align_reverse_transform(complete_shape, alpha, beta, gamma, s, tx, ty, tz)
    reshaped_complete_shape = complete_shape;

    Rx = @(alpha) [1, 0, 0; 0, cos(alpha), -sin(alpha); 0, sin(alpha), cos(alpha)];
    Ry = @(beta) [cos(beta), 0, sin(beta); 0, 1, 0; -sin(beta), 0, cos(beta)];
    Rz = @(gamma) [cos(gamma), -sin(gamma), 0; sin(gamma), cos(gamma), 0; 0, 0, 1];
    R = @(alpha, beta, gamma) Rz(gamma) * Ry(beta) * Rx(alpha);

 
    inv_translated_shape = reshaped_complete_shape - [tx, ty, tz];
    inv_scaled_shape = inv_translated_shape / s;

    inv_rotated_shape = (R(alpha, beta, gamma)' * inv_scaled_shape')';

    original_shape = inv_rotated_shape;
end