
function reprojected_points = reproject_points(points3D, camMatrix)
    points3D_hom = [points3D, ones(size(points3D, 1), 1)];
    points2D_hom = (camMatrix * points3D_hom')';
    reprojected_points = [points2D_hom(:, 1) ./ points2D_hom(:, 3), points2D_hom(:, 2) ./ points2D_hom(:, 3)];
end