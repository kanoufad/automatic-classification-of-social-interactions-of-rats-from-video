function points3D_combined = direct_lin_svd_w_nan(points_c1, points_c2, points_c3, points_c4)
    global camMatrix1 camMatrix2 camMatrix3 camMatrix4
    camMatrices = {camMatrix1, camMatrix2, camMatrix3, camMatrix4};
    points2D = {points_c1, points_c2, points_c3, points_c4};
    points3D_combined = NaN(size(points_c1, 1), 3);
    
    for pointIndex = 1:size(points_c1, 1)
        A = [];
        valid_views = 0;
        for view = 1:4
            point = points2D{view}(pointIndex, :);
            if ~any(isnan(point))
                M = camMatrices{view};
                A = [A;
                     point(1)*M(3,:) - M(1,:);
                     point(2)*M(3,:) - M(2,:)];
                valid_views = valid_views + 1;
            end   
        end 
        if valid_views>=2 && ~isempty(A)
            [~, ~, V] = svd(A);
            X_homogeneous = V(:, end);
            points3D_combined(pointIndex, :) = X_homogeneous(1:3) / X_homogeneous(4);
        else
            points3D_combined(pointIndex, :) = nan;
    end
end