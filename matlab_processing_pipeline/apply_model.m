function [predicted_labels] = apply_model(scaled_points_3d_r1, scaled_points_3d_r2, n_frames, speed_threshold, acc_threshold, min_approach_distance, ...
    min_distance_after, cooldown_frames, features, thresholdh2h, thresholdh2b, ...
    std_threshold,min_distance_passive,cooldown_frames_passive, passive_speed_threshold)

    [approaching, approach_rat, speed1, speed2, acceleration1, acceleration2] = is_approaching(scaled_points_3d_r1, scaled_points_3d_r2, n_frames, speed_threshold, min_approach_distance, min_distance_after, cooldown_frames, features);
    
    contact_threshold = thresholdh2b;
    threshold_H2H = thresholdh2h;
    threshold_H2Base = contact_threshold;
    threshold_angle1 = 0;
    threshold_angle2 = 0;
    
    for i = 2:n_frames
        % disp(i)
        if ~isempty(scaled_points_3d_r1{i})
            [H2H_Contact(i), H1Base2_Contact(i), H2Base1_Contact(i), the_act_angle_b1(i), the_act_angle_b2(i)] = detect_contacts(features(i), scaled_points_3d_r1{i}, scaled_points_3d_r2{i}, threshold_H2H, threshold_H2Base, threshold_angle1, threshold_angle2);
        end
    end
    passive = is_passive(scaled_points_3d_r1, scaled_points_3d_r2, min_distance_passive,n_frames,std_threshold,cooldown_frames_passive,passive_speed_threshold,vertcat(features.speed1),vertcat(features.speed2));
    % rearing = is_rearing(scaled_points_3d_r1, scaled_points_3d_r2, passive_speed_threshold,n_frames);
    window_size = 1;
    
    H2H_Contact_aggregated = zeros(length(H2H_Contact), 1);
    H1Base2_Contact_aggregated = zeros(length(H1Base2_Contact), 1);
    H2Base1_Contact_aggregated = zeros(length(H2Base1_Contact), 1);
    
    for i = 1:window_size:length(H2H_Contact)
        if any(H2H_Contact(i:min(i+window_size-1, end)))
            H2H_Contact_aggregated(i) = 1;
        end
        if any(H1Base2_Contact(i:min(i+window_size-1, end)))
            H1Base2_Contact_aggregated(i) = 1;
        end
        if any(H2Base1_Contact(i:min(i+window_size-1, end)))
            H2Base1_Contact_aggregated(i) = 1;
        end
    end
    predicted_labels = zeros(1,length(H2H_Contact_aggregated));
    for i = 1:length(H2H_Contact)
        if passive(i) == 1
            predicted_labels(i) = 66;
        end
        % if rearing(i) == 1
        %     predicted_labels(i) = 77;
        % end
        % if rearing(i) == 2
        %     predicted_labels(i) = 77;
        % end
        if approaching(i) == 1
            if approach_rat(i) == 1
                predicted_labels(i) = 33;
            elseif approach_rat(i) == 2
                predicted_labels(i) = 44;
            end
        end
        if H2H_Contact(i) == 1
            predicted_labels(i) = 99;
        end
        if H1Base2_Contact(i) == 1
            predicted_labels(i) = 11;
        end
        if H2Base1_Contact(i) == 1
            predicted_labels(i) = 22;
        end
        if H1Base2_Contact(i) == 1 && H2Base1_Contact(i) == 1
            predicted_labels(i) = 121;
        end
        if any([all(isnan(scaled_points_3d_r1{i})), all(isnan(scaled_points_3d_r2{i}))])
            predicted_labels(i) = 1004;
        end
    end
end