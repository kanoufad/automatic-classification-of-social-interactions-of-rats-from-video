function [E, ptransf, s, tx, ty, tz] = allign_tranf_function(angles, x, y, z, xref, yref, zref, w, n)

    x = x(:)'; y = y(:)'; z = z(:)';
    
    Rx = @(alpha) [1, 0, 0; 0, cos(alpha), -sin(alpha); 0, sin(alpha), cos(alpha)];
    Ry = @(beta) [cos(beta), 0, sin(beta); 0, 1, 0; -sin(beta), 0, cos(beta)];
    Rz = @(gamma) [cos(gamma), -sin(gamma), 0; sin(gamma), cos(gamma), 0; 0, 0, 1];
    R = @(alpha, beta, gamma) Rz(gamma) * Ry(beta) * Rx(alpha);
    
    Rot = R(angles(1), angles(2), angles(3));
    p_rot = Rot * [x; y; z];
    
    w = w(:);
    
    A = [sum(w' .* sum(p_rot.^2, 1)), sum(w' .* p_rot(1, :)), sum(w' .* p_rot(2, :)), sum(w' .* p_rot(3, :));
         sum(w' .* p_rot(1, :)), n, 0, 0;
         sum(w' .* p_rot(2, :)), 0, n, 0;
         sum(w' .* p_rot(3, :)), 0, 0, n];
    b = [sum(w' .* (xref .* p_rot(1, :) + yref .* p_rot(2, :) + zref .* p_rot(3, :)));
         sum(w' .* xref);
         sum(w' .* yref);
         sum(w' .* zref)];
    
    q = A \ b;
    s = q(1); tx = q(2); ty = q(3); tz = q(4);
    
    ptransf = s * p_rot + [tx; ty; tz];
    
    E = sum(w' .* sum(([xref, yref, zref]' - ptransf).^2, 1));
end
