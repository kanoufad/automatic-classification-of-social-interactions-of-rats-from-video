%% REPROJECTION ERROR 3D POINT OPTIMATIZATION:

function [points3D_optimized, error_optimatization] = reprojection_error_3D_point_optimatization(points3D_frame, points_2D_c1, points_2D_c2, points_2D_c3, points_2D_c4, mu)
    points3D_frame = points3D_frame;
    error = reprojection_error(points3D_frame, points_2D_c1, points_2D_c2, points_2D_c3, points_2D_c4, mu);
    
    initial_guess = points3D_frame;
    
    options = optimoptions('lsqnonlin','Algorithm','levenberg-marquardt','MaxIterations',10e+02,'MaxFunctionEvaluations',1e6,'FunctionTolerance',1e-10);
    fun = @(p) reprojection_error(reshape(p, size(initial_guess)),  points_2D_c1, points_2D_c2, points_2D_c3, points_2D_c4, mu);
    adjusted_points3D_vector = lsqnonlin(fun, initial_guess(:), [], [], options);
    adjusted_points3D = reshape(adjusted_points3D_vector, size(initial_guess));
    error_adjusted = reprojection_error(adjusted_points3D,  points_2D_c1, points_2D_c2, points_2D_c3, points_2D_c4, mu);
    points3D_optimized = adjusted_points3D;
    error_optimatization = [error, error_adjusted];
end