function nose_coord = compute_nose_3D(points, scale_factor)
    ear1 = 2;
    ear2 = 3;
    spine1 = 4;
    
    ear1_coord = points(ear1, :);
    ear2_coord = points(ear2, :);
    spine1_coord = points(spine1, :);
    
    mid_point = (ear1_coord + ear2_coord) / 2;
    v = mid_point - spine1_coord;
    v = v / norm(v);
    
    distance = norm(spine1_coord - mid_point);
    
    nose_coord = spine1_coord + scale_factor * distance * v;
end