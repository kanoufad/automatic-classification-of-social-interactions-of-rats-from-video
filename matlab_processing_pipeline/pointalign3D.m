function [ptransf, alpha, beta, gamma, s, tx, ty, tz] = pointalign3D(pref, p, w)
    
    nan_idx = isnan(p);
    pref(nan_idx) = [];
    p(nan_idx) = [];
    remove_w = sum(nan_idx)/3;
    w(1:remove_w) = [];

    x = p(1:3:end); y = p(2:3:end); z = p(3:3:end);
    xref = pref(1:3:end); yref = pref(2:3:end); zref = pref(3:3:end);
    n = length(x);
    
    alpha0 = 0; beta0 = 0; gamma0 = 0;
    angles = fminunc(@(ang) allign_tranf_function(ang, x, y, z, xref, yref, zref, w, n), [alpha0, beta0, gamma0]);
    alpha = angles(1); beta = angles(2); gamma = angles(3);
    
    [E, ptransf, s, tx, ty, tz] = allign_tranf_function(angles, x, y, z, xref, yref, zref, w, n);
end