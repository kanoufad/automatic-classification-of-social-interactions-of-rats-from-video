function valid_learning_coordinates_3D = process_by_likelihood(points_3d, likelihood_c1,likelihood_c2,likelihood_c3,likelihood_c4,likelihood_threshold,num_markers)
    full_threshold = likelihood_threshold*num_markers;
    if sum(likelihood_c1) > full_threshold && sum(likelihood_c2) > full_threshold && sum(likelihood_c3) > full_threshold && sum(likelihood_c4) > full_threshold
        valid_learning_coordinates_3D = points_3d;
    else
        valid_learning_coordinates_3D = NaN;
    end
end
