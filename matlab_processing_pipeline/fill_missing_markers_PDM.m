function unaligned_shape = fill_missing_markers_PDM(shape_to_fill, mean_shape, pc,pref,weights_p)
    incomplete_shape = [];
    for i = 1:size(shape_to_fill,1)
        incomplete_shape = [incomplete_shape, shape_to_fill(i,:)];
    end
    missing_idx = isnan(incomplete_shape);
    [ptransf, alpha, beta, gamma, s, tx, ty, tz] = pointalign3D(pref, incomplete_shape, weights_p);
    
    aligned_shape_temp = ptransf';
    incomplete_shape_aligned = [];
    cnt = 1;
    for i = 1:size(shape_to_fill,1)
        if isnan(shape_to_fill(i,:))
            incomplete_shape_aligned = [incomplete_shape_aligned, [nan, nan, nan]];
        else
            incomplete_shape_aligned = [incomplete_shape_aligned, aligned_shape_temp(cnt,:)];
            cnt = cnt + 1;
        end
    end
    
    observed_idx = ~missing_idx;
    % 
    b = pc(observed_idx, :) \ (incomplete_shape_aligned(observed_idx)' - mean_shape(observed_idx));
    
    complete_shape = mean_shape + pc * b;
    
    complete_shape(observed_idx) = incomplete_shape_aligned(observed_idx);
    
    reshape_complete_shape = [complete_shape(1:3:end), complete_shape(2:3:end), complete_shape(3:3:end)];
    unaligned_shape = align_reverse_transform(reshape_complete_shape, alpha, beta, gamma, s, tx, ty, tz);

end