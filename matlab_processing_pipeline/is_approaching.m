% APROACH
function [state, state_rat, speed1, speed2, acceleration1, acceleration2] = is_approaching(rat1_points, rat2_points, num_frames, speed_threshold, min_approach_distance, min_distance_after, cooldown_frames, features)
    state = zeros(num_frames,1); % 0 = no change, 1 = approaching
    state_rat = zeros(num_frames,1);

    speed1 = vertcat(features.speed1);
    speed2 = vertcat(features.speed2);
    acceleration1 = [diff(speed1)];
    acceleration2 = [diff(speed2)];

    cooldown_counter = 0; 

    for i = 1:num_frames-cooldown_frames
        if cooldown_counter > 0
            cooldown_counter = cooldown_counter - 1;
        end

        curr_dist = norm(mean(rat1_points{i},1) - mean(rat2_points{i},1));
        future_dist = norm(mean(rat1_points{i+cooldown_frames},1) - mean(rat2_points{i+cooldown_frames},1));
        if future_dist + min_distance_after < curr_dist && curr_dist > min_approach_distance
            sum_direction_rat1 = 0;
            sum_direction_rat2 = 0;
            
     
            direction_rat1 = mean(rat1_points{i+cooldown_frames},1) - mean(rat1_points{i},1);
            direction_rat2 = mean(rat2_points{i+cooldown_frames},1) - mean(rat2_points{i},1);

            direction_rat1 = direction_rat1/norm(direction_rat1);
            direction_rat2 = direction_rat2/norm(direction_rat2);

            sum_direction_rat1 = sum_direction_rat1/(cooldown_frames-1);
            sum_direction_rat2 = sum_direction_rat2/(cooldown_frames-1);

            direction_rat1_to_rat2 = mean(rat2_points{i},1) - mean(rat1_points{i},1);
            direction_rat1_to_rat2 = direction_rat1_to_rat2/norm(direction_rat1_to_rat2);

            dot_product_rat1 = dot(direction_rat1, direction_rat1_to_rat2);
            dot_product_rat2 = dot(direction_rat2, -direction_rat1_to_rat2);

            if dot_product_rat1 > dot_product_rat2 && speed1(i) > speed_threshold

                state(i+ceil(cooldown_frames/2):i+cooldown_frames) = 1;
                state_rat(i+ceil(cooldown_frames/2):i+cooldown_frames) = 1;
            elseif dot_product_rat1 < dot_product_rat2 && speed2(i) > speed_threshold

                state(i+ceil(cooldown_frames/2):i+cooldown_frames) = 1;
                state_rat(i+ceil(cooldown_frames/2):i+cooldown_frames) = 2;
            end

            cooldown_counter = cooldown_frames; 
        end
    end
end

