% CONTACT
function [H2H_Contact, H1Base2_Contact, H2Base1_Contact, the_act_angle_b1, the_act_angle_b2] = detect_contacts(features, rat1_points, rat2_points, threshold_H2H, threshold_H2Base, threshold_angle1, threshold_angle2)

    H2H_Contact = features.dist_Head2Head < threshold_H2H;
    
    H2Base1_Contact = features.dist_Nose2Tailbase1 < threshold_H2Base;
    
    H1Base2_Contact = features.dist_Nose1Tailbase2 < threshold_H2Base;
    
    direction_H2H = rat2_points(1,:) - rat1_points(1,:); 
    direction_H1 = rat1_points(1,:) - rat1_points(4,:);
    direction_H2 = rat2_points(1,:) - rat2_points(4,:);
    direction_B1 = rat1_points(9,:) - rat1_points(1,:);
    direction_B2 = rat2_points(9,:) - rat2_points(1,:);

    direction_H2H = direction_H2H/norm(direction_H2H);
    direction_H1 = direction_H1/norm(direction_H1);
    direction_H2 = direction_H2/norm(direction_H2);
    direction_B1 = direction_B1/norm(direction_B1);
    direction_B2 = direction_B2/norm(direction_B2);
    
    opposite_dir = dot(direction_H1, direction_H2) < 0;
    same_dir = abs(dot(direction_H1, direction_H2)) > 0.85;

    the_act_angle_b1 = abs(dot(direction_H2, direction_B1));
    the_act_angle_b2 = abs(dot(direction_H1, direction_B2));
end

