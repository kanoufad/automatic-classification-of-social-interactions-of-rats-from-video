function save_points_3d_as_csv(scaled_points_3d_r1, n_frames, model_data_folder, start_time, end_time,id)
    n_points = size(scaled_points_3d_r1, 1);
    n_dims = size(scaled_points_3d_r1, 2);

    reshaped_2D_array=[];
    for i = 1:n_frames
        for j = 1:n_points
            reshaped_2D_array = [reshaped_2D_array; scaled_points_3d_r1(j,:,i)'];
        end
    end
    flattened_array = reshaped_2D_array;
    
    frame_idx = repelem((1:n_frames)', n_points * n_dims);
    point_idx = repmat(repelem((1:n_points)', n_dims), n_frames, 1);
    dim_idx = repmat(repmat((1:n_dims)', n_points, 1), n_frames, 1);
    
    final_array = [frame_idx, point_idx, dim_idx, flattened_array];
    
    coordinates_folder = fullfile(model_data_folder, 'deep_coordinates_3D');
    if ~exist(coordinates_folder, 'dir')
        mkdir(coordinates_folder);
    end
    coordinates_folder_mat = fullfile(model_data_folder, 'deep_coordinates_3D_mat');

    if ~exist(coordinates_folder_mat, 'dir')
            mkdir(coordinates_folder_mat);
    end
    if id == 1
        csv_file_path = fullfile(coordinates_folder, sprintf("%d_%d_coordinates_3D_rat1.csv", start_time, end_time));
        save(fullfile(coordinates_folder_mat, sprintf("%d_%d_coordinates_3D_rat1.mat", start_time, end_time)),"scaled_points_3d_r1");

    elseif id == 2
        csv_file_path = fullfile(coordinates_folder, sprintf("%d_%d_coordinates_3D_rat2.csv", start_time, end_time));
        save(fullfile(coordinates_folder_mat, sprintf("%d_%d_coordinates_3D_rat2.mat", start_time, end_time)),"scaled_points_3d_r1");

    end
    writematrix(final_array, csv_file_path);
    
    disp('coordinates saved');

    

end
