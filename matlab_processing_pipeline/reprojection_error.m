%% REPROJECTION ERROR

function error = reprojection_error(points3D, cam1points, cam2points, cam3points, cam4points, mu)
    total_error = 0;
    points3D_frame = points3D;
    for view = 1:4
        points2D = reproject_view_frame(view, points3D_frame);
        if view == 1
            points_origin = cam1points;
        elseif view == 2
            points_origin = cam2points;
        elseif view == 3
            points_origin = cam3points;
        elseif view == 4
            points_origin = cam4points;
        end
        % total_error = total_error + sum(sum((points2D - points_origin).^2,'omitmissing'),'omitmissing');
        
        total_error = total_error + mu * sum(sqrt((points2D(:,1) - points_origin(:,1)).^2 + (points2D(:,2) - points_origin(:,2)).^2),'omitmissing');
    end
    error = total_error;
end

