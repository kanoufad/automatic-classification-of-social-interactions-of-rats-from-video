%% REPROJECTION ERROR 3D SWAP OPTIMATIZATION:

function [points_corrected_r1, points_corrected_r2, best_indices_rat1, best_indices_rat2, min_error,last_valid_idx] = reprojection_error_3D_swap_optimatization(indices_view_rat1, indices_view_rat2, ...
    points_2D_r1_c1, points_2D_r1_c2, points_2D_r1_c3, points_2D_r1_c4, ...
    points_2D_r2_c1, points_2D_r2_c2, points_2D_r2_c3, points_2D_r2_c4, ...
    points_cor_before_r1, points_cor_before_r2,idx,last_valid_idx_main)
    min_error = Inf;
    min_total_movement = Inf;
    best_indices_rat1 = [0,0,0,0];
    best_indices_rat2 = [0,0,0,0];
    
    all_combinations = dec2bin(0:15) - '0';  
    
    for j = 1:16  % For each combination
        combination = all_combinations(j, :);
        
        points_2D_rat1 = {points_2D_r1_c1, points_2D_r1_c2, points_2D_r1_c3, points_2D_r1_c4};
        points_2D_rat2 = {points_2D_r2_c1, points_2D_r2_c2, points_2D_r2_c3, points_2D_r2_c4};
        
        for i = 1:4
            if combination(i) == 0
                points_2D_rat1{i} = points_2D_rat1{i};
                points_2D_rat2{i} = points_2D_rat2{i};
            else
                temp = points_2D_rat1{i};
                points_2D_rat1{i} = points_2D_rat2{i};
                points_2D_rat2{i} = temp;
            end
        end
        
        total_movement = 0;
        points3D_frame_rat1 = direct_lin_svd_w_nan(points_2D_rat1{1}, points_2D_rat1{2}, points_2D_rat1{3}, points_2D_rat1{4});
        points3D_frame_rat2 = direct_lin_svd_w_nan(points_2D_rat2{1}, points_2D_rat2{2}, points_2D_rat2{3}, points_2D_rat2{4});
        max_frames = 30;
        
        k = idx - last_valid_idx_main ;
        
        points3D_before_rat1 = direct_lin_svd_w_nan(points_cor_before_r1{idx-k}{1}, points_cor_before_r1{idx-k}{2}, points_cor_before_r1{idx-k}{3}, points_cor_before_r1{idx-k}{4});
        points3D_before_rat2 = direct_lin_svd_w_nan(points_cor_before_r2{idx-k}{1}, points_cor_before_r2{idx-k}{2}, points_cor_before_r2{idx-k}{3}, points_cor_before_r2{idx-k}{4});
        movement_in_3D_r1 = sum(sqrt(sum((points3D_frame_rat1 - points3D_before_rat1).^2, 2,'omitnan')));
        movement_in_3D_r2 = sum(sqrt(sum((points3D_frame_rat2 - points3D_before_rat2).^2, 2,'omitnan')));
        total_movement = total_movement + movement_in_3D_r1 + movement_in_3D_r2;
       
        if all(all(isnan(points3D_frame_rat1))) == 0 && all(all(isnan(points3D_frame_rat2))) == 0
            last_valid_idx = idx;
        else
            last_valid_idx = last_valid_idx_main;
        end
        
        mu = 1;
        error_r1 = reprojection_error(points3D_frame_rat1, points_2D_rat1{1}, points_2D_rat1{2}, points_2D_rat1{3}, points_2D_rat1{4}, mu);
        error_r2 = reprojection_error(points3D_frame_rat2, points_2D_rat2{1}, points_2D_rat2{2}, points_2D_rat2{3}, points_2D_rat2{4}, mu);
        error = error_r1 + error_r2;
        % if idx == 205
        %     disp(combination)
        %     disp(total_movement)
        % end
        
        if error <= min_error && total_movement < min_total_movement
            min_error = error;
            best_indices_rat1 = combination;
            best_indices_rat2 = 1 - combination;  
            
            min_total_movement = total_movement;
           
        end
        
    end
    points_2D_rat1_not = {points_2D_r1_c1, points_2D_r1_c2, points_2D_r1_c3, points_2D_r1_c4};
    points_2D_rat2_not = {points_2D_r2_c1, points_2D_r2_c2, points_2D_r2_c3, points_2D_r2_c4};
    
    for idx = 1:4
        if best_indices_rat1(idx) == 1
            points_corrected_r1{idx} = points_2D_rat2_not{idx};
            points_corrected_r2{idx} = points_2D_rat1_not{idx};
        else
            points_corrected_r1{idx} = points_2D_rat1_not{idx};
            points_corrected_r2{idx} = points_2D_rat2_not{idx};
        end
    end
end

