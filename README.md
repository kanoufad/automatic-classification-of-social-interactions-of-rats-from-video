# Automatic Classification of Social Interactions of Rats from Video

Alzheimer's disease (AD) is the most common cause of dementia. Disruption in social behavior is often an early symptom of AD, typically appearing before the onset of cognitive domain symptoms.

The main focus of this work is the creation of algorithms for the automatic detection and classification of various types of specific social interactions in transgenic TgF344-AD rats - an animal model of Alzheimer's disease. The rats were observed using cameras from multiple views, which allowed the assessment of the rat's motion in a 3D coordinate frame. The rats were observed during a social interaction test, in which unfamiliar individuals primarily examine each other by sniffing the anogenital area, head, or the rest of the body in a closed arena. 

The work presents the implementation of suitable action recognition methods, the creation of datasets comprehending the rat's actions, utilizing software such as DeepLabCut to obtain the pose of the rats, and utilizing the multiple camera views to improve the rat's estimated pose and identity tracking. Our methods managed to classify the rat's actions reliably and, if applicable, the initiator of the action.
The work also includes a statistical analysis to compare the social interactions in TgF344-AD and F344 rats (control group), using the predictions from the developed methods on the obtained video footage. We did not find any significant differences between the TgF344-AD and F344 in denoted actions.

## Introduction

The provided code and files follow the implementation and methods denoted in the master's thesis Automatic Classification of Social Interactions of Rats from Video. The code includes the implemented methods such as data-processing, methods/models implementation etc. The provided repository can be used for the analysis of the recorded video-subsets and inspecting the denoted methods. The code does not include the individual experiments and statistical analysis employed in the work. As for the files, we include examples of video predictions of the denoted methods, an example of recorded video subset, project configuration of employed DeepLabCut algorithm, and the pretrained model weights of the denoted deep learning models for the action recognition.

## Description
### Folder: matlab_processing_pipeline

The folder includes the code of the processing pipeline in MATLAB - includes the implementation of the methods in Data Section of the work such as spatial and temporal imputation, outlier detection etc.
The processing pipeline is also used for processing the whole video subset - at the start of the file include data_folder to given video subset. The pipeline then runs the trained DLC model (a python call from MATLAB) and estimates the pose and tracking for the given subset (analysis of video from four camera views). The user can adjust the lenght of segments to be analysed from the subset, aswell a length and other parameters. 
After the DLC analysis, the user is prompted to identify the two rats - a simple MATLAB gui to click on the rat that is being identified as Rat 1. 
After denoting the identity for each segment, the pipeline runs the denoted methods, and results with action predictions .txt files - again calling python to run the trained deep learning models in a python environment. The pipeline also includes saving the corrected and computed 3D coordinates, and predictions from the rule-based models.

The processing pipeline file is: pipeline_analyze_subset_w_DLC.m

### Folder: quad_view_pyrealsense_record

Python code using pyrealsense2 library to use the quad view camera setup (Camera type: Intel® RealSenseTM D435).

### Folder: models_developing_torch_pipeline

This folder includes the setup, training and implementation of the implemented methods/models using pytorch. 
data_loader.py includes code for building the train and validation sets for the given model implementation, fetching item (a modality for given sequence window) to the batch. Each item is processed in features.py (such as graph topology, TSRJ images, etc.).

predictions.py is called from withing the MATLAB pipeline to obtain the action predictions using the resulting trained models on the corrected sequence windows. 
Note I: each corrected sequence is temporaly saved using MATLAB and loaded correspondingly by predictions.py
Note II: change the paths of the trained model weights in predictions.py to the corresponding paths of trained models downloaded within this repository.

Model names and their corresponding methods in the thesis:

model_image.py: CNN-LSTM denoted in Section 3.3.5

model_gcn_simple.py: ST GCN (parallel streams) denoted in Section 3.3.1

model_gcn_series.py: ST GCN (series streams) denoted in Section 3.3.1

model_feature.py: 1D CNN denoted in Section 3.3.8

model_cuboid.py: 3D CNN denoted in Section 3.3.7

model_coord_image_pivot.py: TSRJI-CNN Pivot denoted in Section 3.3.4

model_coord_image.py: TSRJI-CNN denoted in Section 3.3.3

model_combined_v1_pretrained.py, model_concat1.py,model_concat3.py: the multi-modal networks denoted in Section 3.3.9, where model_combined_v1_pretrained.py resulted in the final MMc3 denoted and used network for the prediction of actions between the rats. 

### Folder: dlc_subset_analysis

Python code called within the MATLAB processing pipeline to run the DLC analysis.
Note: change the path to the DLCrnet-MS5 trained model - refer to following section.
### Folder: deeplabcut_project_network_configurations

DLC project configuration used to train the resulting DLCRnet-MS5, the DLCRnet-MS5 trained weights. The folder does not include the videos used to train the network.

### Folder: predictions_video

An example of the action predictions on 10 second video segments (described in Section 4.19.1).

### Folder: subset_video_example

An example of segmented video subset (5-105 seconds) AD1-AD3 from all four camera views.

### Folder: action_recog_trained_weights

Trained model weights of the MMc3 multi-modal network for recognizing the denoted actions, and TSRJI-CNN model weights to classify the action initiator (if applicable).

## Authors and acknowledgment
I would like to express my deepest appreciation to my supervisors, RNDr. David Levcik, Ph.D., and Prof. Dr. Ing. Jan Kybic. Their invaluable guidance, patience, and ability to provide insightful solutions to any question or challenge have significantly shaped my work, thinking and experience.

Fádi Kanout

### Contact information
In case of inacuracies contact me on fkanout98@gmail.com or kanoufad@fel.cvut.cz.


