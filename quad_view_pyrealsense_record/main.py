
import pyrealsense2 as rs
import numpy as np
import cv2


if __name__ == '__main__':

    ctx = rs.context()
    # print(device)
    # sensor = device.first_color_sensor()
    # sensor.set_option(rs.option.inter_cam_sync_mode, 2.0)
    detected_cameras = []
    pipelines = []
    configs = []
    color_writers = []
    color_paths = ['E:/data_14month/cam1.mp4', 'E:/data_14month/cam2.mp4', 'E:/data_14month/cam3.mp4',
                   'E:/data_14month/cam4.mp4']
    # print(rs.intrinsics())
    for i in range(0, 4):
        print(i)
        detected_camerax = ctx.devices[i].get_info(rs.camera_info.serial_number)
        pipelinex = rs.pipeline()
        configx = rs.config()
        configx.enable_device(detected_camerax)
        pipeline_wrapper = rs.pipeline_wrapper(pipelinex)
        pipeline_profile = configx.resolve(pipeline_wrapper)

        color_writer_x = cv2.VideoWriter(color_paths[i], cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (1280, 720), 1)
        configx.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 30)

        configs.append(configx)
        pipelines.append(pipelinex)
        color_writers.append(color_writer_x)
    # Start streaming
    for c in range(0, np.shape(pipelines)[0]):
        pipelines[c].start(configs[c])

    try:
        while True:
            frames1 = pipelines[0].wait_for_frames()
            color_frame1 = frames1.get_color_frame()

            frames2 = pipelines[1].wait_for_frames()
            color_frame2 = frames2.get_color_frame()

            frames3 = pipelines[2].wait_for_frames()
            color_frame3 = frames3.get_color_frame()

            frames4 = pipelines[3].wait_for_frames()
            color_frame4 = frames4.get_color_frame()

            color_image1 = np.asanyarray(color_frame1.get_data())
            color_image2 = np.asanyarray(color_frame2.get_data())
            color_image3 = np.asanyarray(color_frame3.get_data())
            color_image4 = np.asanyarray(color_frame4.get_data())

            color_writers[0].write(color_image1)
            color_writers[1].write(color_image2)
            color_writers[2].write(color_image3)
            color_writers[3].write(color_image4)

            # Show images cam 1
            cv2.namedWindow('RealSense1', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense1', color_image1)

            # Show images cam 2
            cv2.namedWindow('RealSense2', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense2', color_image2)

            # Show images cam 3
            cv2.namedWindow('RealSense3', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense3', color_image3)

            # Show images cam 4
            cv2.namedWindow('RealSense4', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense4', color_image4)

            if cv2.waitKey(1) == ord("q"):
                break

    finally:
        # Stop streaming
        for c in range(0, np.shape(pipelines)[0]):
            color_writers[c].release()
            pipelines[c].stop()

