import sys
import deeplabcut
import os

os.environ['TF_GPU_ALLOCATOR'] = 'cuda_malloc_async'


def main(video_path, destfolder_results):
    config_path = r'C:\Users\fgu032lab034\Desktop\dlc_new-final-2023-08-18\config.yaml'

    shuffle = 6
    deeplabcut.analyze_videos(config_path, [video_path], shuffle=shuffle, save_as_csv=True, destfolder=destfolder_results)


if __name__ == '__main__':
    video_path = sys.argv[1]
    destfolder_results = sys.argv[2]
    main(video_path, destfolder_results)
    output_file_path = os.path.join(destfolder_results, "output_file.txt")
    with open(output_file_path, "w") as f:
        f.write("DeepLabCut analysis completed successfully.")
